<?php

declare(strict_types=1);

namespace Microframe\Application;

use Composer\Autoload\ClassLoader;
use Microframe\Application\Exception\ParameterMissmatchException;
use Microframe\Common\Exception\MethodNotFoundException;
use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Mvc\Model;
use Microframe\Provider\Provider;
use Microframe\Proxy\Exception\ControllerNotFoundException;
use ReflectionClass;
use ReflectionMethod;
use ReflectionParameter;
use ReflectionProperty;
use Microframe\Validable\RequestValidator;

final class Autowire implements RegistableInterface
{
    use RegisterTrait;

    private DI $di;

    public function __construct()
    {
        $this->di = new DI();
    }

    public function wireClass(object|string $class, array $constuctorParams = []): object
    {
        if (is_string($class)) {
            $class = new $class(...$constuctorParams);
        }
        $reflection = new ReflectionClass($class);
        foreach ($reflection->getProperties(ReflectionProperty::IS_PROTECTED | ReflectionProperty::IS_PRIVATE) as $property) {
            $loadedClass = $this->getTypedParameter($property);
            if (!$loadedClass) {
                continue;
            }
            $property->setValue($class, $loadedClass);
        }
        return $class;
    }

    public function wireMethod(string $object, string $method, array $source = []): array
    {
        $buildedValues = [];
        $function = $this->getMethodReflection($object, $method);
        foreach ($function->getParameters() as $parameter) {
            if ($parameter->hasType()) {
                $loadedClass = $this->getTypedParameter($parameter, $source);
                if (!$loadedClass) {
                    continue;
                }
                $buildedValues[] = $loadedClass;
            } elseif (!empty($source)) {
                $buildedValues[] = $source[$parameter->getName()] ?? null;
            } else {
                throw new ParameterMissmatchException($object, $method, $parameter->getName());
            }
        }

        return $buildedValues;
    }

    private function getTypedParameter(ReflectionProperty|ReflectionParameter $parameter, array $source = []): mixed
    {
        $className = $parameter->getType()->getName();
        if ($this->isModel($className)) {
            $loadedClass = $this->matchNameToModel($className, $source);
        } elseif ($class = $this->getFromDiByType($className)) {
            $loadedClass = $class;
        } elseif ($this->isProvidedParameter($className)) {
            $loadedClass = $this->loadProvided($className);
        } elseif (is_subclass_of($className, RequestValidator::class)) {
            $this->validateResource($className, $source);
            $loadedClass = $this->convertIfNeed($parameter, $source);
        }
        else {
            $loadedClass = $this->convertIfNeed($parameter, $source);
        }
        return $loadedClass;
    }

    /**
     * Get the method reflection.
     *
     * @return ReflectionMethod
     */
    private function getMethodReflection(string $className, string $functionName): ReflectionMethod
    {
        if (!class_exists($className)) {
            throw new ControllerNotFoundException($className);
        }

        if (!method_exists($className, $functionName)) {
            throw new MethodNotFoundException($className, $functionName);
        }
        return new ReflectionMethod($className, $functionName);
    }

    /**
     * Convert a parameter to type if need.
     *
     * @param ReflectionParameter|ReflectionProperty $parameter
     * @return mixed converted to type
     */
    protected function convertIfNeed(ReflectionParameter|ReflectionProperty $parameter, array $source = []): mixed
    {
        $name = $parameter->getName();
        $type = $parameter->getType()->getName();
        if (class_exists($type)) {
            if (empty($source) and !is_subclass_of($type, RequestValidator::class)) {
                return new $type();
            }
            return $this->di->conversionService->convert($source, $type);
        }
        if (!isset($source[$name])) {
            return null;
        }
        $value = $source[$name];
        settype($value, $type);
        return $value;
    }

    private function matchNameToModel(string $modelClassName, array &$source): ?Model
    {
        $className = explode(Dispatcher::NAMESPACE_DELIMITER, $modelClassName);
        $className = lcfirst(array_pop($className));
        if (isset($source[$className])) {
            return $modelClassName::find($source[$className]);
        }

        $modelName = new $modelClassName();
        $primaryKey = $modelName->getKeyName();
        $model = null;

        if (isset($source[$primaryKey])) {
            $model = $modelName->find($source[$primaryKey]);
            unset($source[$primaryKey]);
        }

        $key = $className . ucfirst($primaryKey);

        if (isset($source[$key])) {
            $model = $modelName->find($key);
            unset($source[$key]);
        }

        return $model;
    }

    private function isModel(string $className): bool
    {
        return is_subclass_of($className, Model::class);
    }

    private function isProvidedParameter(string $className): bool
    {
        return $this->di->has(Provider::DI_KEY) and
            $this->isProvidedClass($className);
    }

    private function isProvidedClass(string $className): bool
    {
        return $this->di->has(Provider::DI_KEY) and
            isset($this->di->get(Provider::DI_KEY)[$className]);
    }

    private function loadProvided(string $className)
    {
        return $this->loadProvidedClass($className);
    }

    private function loadProvidedClass(string $className)
    {
        $objectName = $this->di->get(Provider::DI_KEY)[$className];
        return $this->wireClass($objectName);
    }

    private function getVendorPath(): string
    {
        return dirname((new ReflectionClass(ClassLoader::class))->getFileName(), 2);
    }

    private function getFromDiByType(string $className): mixed
    {
        if ($this->isProvidedClass($className)) {
            $className = $this->di->get(Provider::DI_KEY)[$className];
        }
        return $this->di->getByType($className);
    }

    private function validateResource(string $className, array $source): void
    {
        $object = new $className();
        $object->validate($source);
    }
}