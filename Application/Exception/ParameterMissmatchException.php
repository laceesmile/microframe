<?php

declare(strict_types=1);

namespace Microframe\Application\Exception;

use Microframe\Common\Exception\MicroframeException;

class ParameterMissmatchException extends MicroframeException
{
    public function __construct(string $objectName, string $method, string $paramName, int $code = 500)
    {
        $this->code = $code;
        $this->message = `Function $method of object $objectName request for invalid $paramName parameters!`;
    }
}