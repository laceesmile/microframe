<?php

declare(strict_types=1);

namespace Microframe\Application;

use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Event\EventTrait;
use Microframe\Http\Response;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Router\Route;

class Dispatcher extends DI implements RegistableInterface
{
    use EventTrait, RegisterTrait;

    const ACTION_SUFFIX = "Action";

    const NAMESPACE_DELIMITER = "\\";

    const DEFINER_FILE_NAME = "App";

    private ?Route $route = null;
    private $controller;
    private string $action;


    public function buildRoute()
    {
        if ($this->route === null) {
            $this->route = $this->router->getRoute($this->request);
        }

        $this->request->setMatchedRoute($this->route);

        return $this;
    }

    public function fire()
    {
        $this->buildRoute();

        $response = $this->prepareFire();
        if ($response instanceof Response) {
            return $response;
        }
        $this->runEvent("beforeExecuteRoute", [$this->route]);
        $action = $this->action;

        ob_start();
        $response = $this->controller->$action(...$this->route->getParams());
        if ($this->has('view')) {
            $this->view->addContent(ob_get_clean());
        }

        return $this->runEvent("afterExecuteRoute", [$response]) ?? $response;
    }

    private function prepareFire()
    {
        $beforeDispatchEventAnswer = $this->runEvent("beforeDispatch", [$this->route]);
        if ($beforeDispatchEventAnswer instanceof Route) {
            $this->route = $beforeDispatchEventAnswer;
        }
        $this->buildModule();
        $controller = $this->route->getController();

        $action = $this->route->getAction() . self::ACTION_SUFFIX;

        $this->controller = new $controller;
        $this->action = $action;
        return $this->runEvent("afterDispatch", [$this->route]);
    }

    private function buildModule()
    {
        $module = $this->route->getBaseNamespace();
        $definerFileName = $this->get("microframeConfig")->definerFileName ?? self::DEFINER_FILE_NAME; 
        $definer = $module . self::NAMESPACE_DELIMITER . $definerFileName;
        if (class_exists($definer)) {
            $definer = new $definer();
            $definer->buildModule();
        }
    }

    /**
     * Get the value of route
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Get the value of controller
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Get the value of action
     */
    public function getAction()
    {
        return $this->action;
    }
}
