<?php

declare(strict_types=1);

namespace Microframe\Di;

use Microframe\Common\NativeArray;
use Microframe\Storage\RuntimeMemory;

final class DiStorage extends RuntimeMemory
{
    public static function get(string $name): ?DiObject
    {
        return self::getStorage()->$name ?? null;
    }
}