<?php

declare(strict_types=1);

namespace Microframe\Di;

final class DiObject
{
    private const TYPE_OBJECT = 1;
    private const TYPE_CALLABLE = 2;
    private const TYPE_OTHER = 3;

    private const OBJECT_TYPE = [
        self::TYPE_OBJECT => "object",
        self::TYPE_CALLABLE => "callable",
        self::TYPE_OTHER => "other",
    ];

    private const OBJECT_TYPE_ASSOC = [
        "object" => self::TYPE_OBJECT,
        "callable" => self::TYPE_CALLABLE,
        "other" => self::TYPE_OTHER,
    ];

    private $value = null;
    private int $type = self::TYPE_OTHER;

    public bool $shared;

    public function build($value, bool $shared = true): self
    {
        $this->value = $value;

        $type = $value instanceof \Closure ? self::OBJECT_TYPE[self::TYPE_CALLABLE] : gettype($value);
        $this->type = self::OBJECT_TYPE_ASSOC[$type] ?? self::TYPE_OTHER;

        $this->shared = $shared;

        return $this;
    }

    public function getValue(array $params = []): mixed
    {
        if ($this->type == self::TYPE_CALLABLE and $this->shared and $this->value instanceof \Closure) {
            $function = $this->value;
            $this->value = $function(...$params);
        } elseif ($this->type == self::TYPE_OBJECT and !$this->shared) {
            $object = $this->value;
            $this->value = new $object(...$params);
        }
        return $this->value ?? null;
    }
}