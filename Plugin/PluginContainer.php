<?php

declare(strict_types=1);

namespace Microframe\Plugin;

use Microframe\Common\ForwardCall;
use Microframe\Storage\RuntimeMemory;

class PluginContainer extends RuntimeMemory
{
    use ForwardCall;
    
    public function __get(string $name): mixed
    {
        return self::get($name);
    }
}