<?php

declare(strict_types=1);

namespace Microframe\Plugin\Exception;

use Microframe\Common\Exception\MicroframeException;

class PluginException extends MicroframeException
{
    
}