<?php

declare(strict_types=1);

namespace Microframe\Interfaces;

interface ProviderInterface
{
    /**
     * Register the providers.
     *
     * @return array
     */
    public function provide(): array;
}