<?php

declare(strict_types=1);

namespace Microframe\Interfaces;

use Microframe\Common\NativeArray;

interface StorageInterface
{
    /**
     * Get the storage instance.
     *
     * @return NativeArray
     */
    public static function getStorage(): NativeArray;
    
    /**
     * Get an element from the storage.
     *
     * @param string $key
     * @return object|null
     */
    public static function get(string $key): mixed;

    /**
     * Set an element into the storage.
     *
     * @param string $key
     * @param object $value
     * @return void
     */
    public  static function set(string $key, object $value): void;

    /**
     * Check if storage has the element.
     *
     * @param string $key
     * @return boolean
     */
    public static function has(string $key): bool;

    /**
     * Remove an element from the storage.
     *
     * @param string $key
     * @return void
     */
    public static function remove(string $key): void;

    /**
     * Clear the memory storage.
     */
    public static function clear();
}