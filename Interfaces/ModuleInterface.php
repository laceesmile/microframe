<?php

declare(strict_types=1);

namespace Microframe\Interfaces;

interface ModuleInterface
{
    /**
     * Setup the module requirements and varaibles.
     *
     * @return void
     */
    public function buildModule();
}