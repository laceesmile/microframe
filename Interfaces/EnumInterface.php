<?php

declare(strict_types=1);

namespace Microframe\Interfaces;

interface EnumInterface
{
    public function get(): int|string|float|object|bool|iterable|callable;
}