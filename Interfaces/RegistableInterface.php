<?php

declare(strict_types=1);

namespace Microframe\Interfaces;

interface RegistableInterface
{
    /**
     * Get the service name.
     * User RegisterTrait to satisfy the interface.
     *
     * @return string
     */
    public static function getRegisterName(): string;
}