<?php

declare(strict_types=1);

namespace Microframe\Html;

interface TagInterface
{
    public function __toString(): string;

    public function tag(string $tag): self;

    public function render(): string;

    public function build(): string;
}