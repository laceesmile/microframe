<?php

declare(strict_types=1);

namespace Microframe\Html;

use Microframe\Html\Flash;
use InvalidArgumentException;
use Microframe\Html\TagInterface;
use Microframe\Common\NativeArray;
use Microframe\DI;

abstract class AbstractTag extends DI implements TagInterface
{
    use Flash;
    
    protected string $html;
    protected string $tag;

    public function __toString(): string
    {
        return $this->render();
    }
    
    public function toString(array $additional = []): string
    {
        return $this->render($additional);
    }

    public function render(array $additional = []): string
    {
        return $this->build($additional);
    }

    public function tag(string $tag): TagInterface
    {
        $this->tag = $tag;

        return $this;
    }

    protected function buildFromArray($params): string
    {
        if (!($params instanceof NativeArray) and !is_array($params)) {
            throw new InvalidArgumentException("HTML tag creating works only with array|NativeArray, " . gettype($params) . " given.", 500);
        }
        $builded = [];
        foreach ($params as $key => $value) {
            if (is_scalar($value)) {
                $builded[] = "$key=\"".htmlspecialchars((string) $value)."\"";
            }
        }
        if (count($builded) == 0) {
            return "";
        }
        return " " . join(" ", $builded);
    }

    protected function wrap(string $htmlAttributes, bool $autoclose = false)
    {
        $html = "<" . $this->tag . $htmlAttributes;
        $close = null;
        if ($autoclose) {
            $close = "/>";
        } else {
            $close = ">";
        }
        $this->html = $html . $close;
        return $this->html;
    }

    public function close(): string
    {
        return "</" . $this->tag . ">";
    }
}