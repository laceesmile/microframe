<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use Microframe\Html\AbstractTag;

class InnerCss extends AbstractTag
{
    private $content = "";

    public function __construct(string $content, array $options = [])
    {
        $this->content = $content;
        $this->options = $options;
        $this->tag("style");
    }

    public function build(): string
    {
        return $this->wrap($this->buildFromArray($this->options)) . $this->content . $this->close();
    }
}