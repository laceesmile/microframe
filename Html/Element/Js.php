<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use Microframe\Html\AbstractTag;

class Js extends AbstractTag
{
    public function __construct(array $options)
    {
        $this->options = $options;
        $this->tag("script");
    }

    public function build(): string
    {
        return $this->wrap($this->buildFromArray($this->options)) . $this->close();
    }
}