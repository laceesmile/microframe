<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use Microframe\Html\AbstractTag;

class InnerJs extends AbstractTag
{
    private $content = "";

    public function __construct(string $content, array $options = [])
    {
        $this->content = $content;
        $this->options = $options;
        $this->tag("script");
    }

    public function build(): string
    {
        return $this->wrap($this->buildFromArray($this->options)) . $this->content . $this->close();
    }
}