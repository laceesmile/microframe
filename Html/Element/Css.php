<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use Microframe\Html\AbstractTag;
use Stringable;

class Css extends AbstractTag implements Stringable
{
    public function __construct(array $options)
    {
        $this->options = $options;
        $this->tag("link");
    }

    public function build(): string
    {
        return $this->wrap($this->buildFromArray($this->options));
    }

    public function __toString(): string
    {
        return $this->build();
    }
}