<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use Microframe\Html\AbstractTag;

class Option extends AbstractTag
{
    protected $value = null;

    public function __construct($content, $options = [])
    {
        $this->content = (string) $content;
        $this->options = $options;
        $this->value = $options["value"] ?? null;
        $this->tag("option");
    }

    public function build(array $additional = []): string
    {
        $allOptions = array_merge($this->options, $additional);
        $this->value = $allOptions["value"] ?? null;
        return $this->wrap($this->buildFromArray($allOptions)) . $this->content . $this->close();
    }

    public function selected()
    {
        $this->options["selected"] = "selected";

        return $this;
    }

    public function addOption(string $name, $value): self
    {
        $this->options[$name] = $value;

        return $this;
    }

    public function setOptions(array $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get the value of value
     */ 
    public function getValue()
    {
        return $this->value;
    }
}