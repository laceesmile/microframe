<?php

declare(strict_types=1);

namespace Microframe\Html\Element;

use JsonSerializable;
use Microframe\Html\AbstractTag;

class Label extends AbstractTag implements JsonSerializable
{
    public function __construct(string $content, ?array $options = [])
    {
        $this->content = $content;
        $this->options = $options;
        $this->tag("label");
    }

    public function build(): string
    {
        return $this->wrap($this->buildFromArray($this->options)) . $this->content . $this->close();
    }

    public function jsonSerialize(): string
    {
        return $this->content;
    }

    /**
     * Set the value of options
     *
     * @return  self
     */ 
    public function addOptions(array $options = []): self
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    public function toString(array $additional = []): string
    {
        return $this->content;
    }
}