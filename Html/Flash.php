<?php

declare(strict_types=1);

namespace Microframe\Html;

use Microframe\Html\Message;
use Microframe\DI;

trait Flash
{
    private ?string $message = null;

    public function getMessage(array $additional = []): ?string
    {
        $di = new DI();
        if (!$di->has('cache')) {
            return $this->message;
        } elseif ($this->get("cache")->has("flashContent" . $this->name)) {
            $message = $di->get("cache")->get("flashContent" . $this->name);
            $html = $message->build($additional);
            $di->get("cache")->remove("flashContent" . $this->name);
            return $html;
        }
        return null;
    }

    public function setMessage(Message $message): void
    {
        $di = new DI();
        if (!$di->has('cache')) {
            $this->message = $message->toString();
        } else {
            $di->get("cache")->set("flashContent" . $this->name, $message);
        }
    }

    public function hasMessage(): bool
    {
        $di = new DI();
        if (!$di->has('cache')) {
            return $this->message == null;
        } else {
            return $di->get("cache")->has("flashContent" . $this->name);
        }
    }


    public function getMessageString(): ?string
    {
        $di = new DI();
        if (!$di->has('cache')) {
            return $this->message;
        } else if ($di->get("cache")->has("flashContent" . $this->name)) {
            $message = $di->get("cache")->get("flashContent" . $this->name);
            $di->get("cache")->remove("flashContent" . $this->name);
            return $message->toString();
        }
        return null;
    }
}
