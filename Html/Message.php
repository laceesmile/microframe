<?php

declare(strict_types=1);

namespace Microframe\Html;

use Microframe\Html\AbstractTag;

class Message extends AbstractTag
{
    protected string $content;
    private string $type;

    public function __construct(string $content, string $type = "danger")
    {
        $this->content = $content;
        $this->type = $type;
        $this->tag("div");
    }

    public function build(array $additional = []): string
    {
        $params = $this->processOptions($additional["class"] ?? null);
        return $this->wrap($this->buildFromArray($params)) . $this->content . $this->close();
    }

    private function processOptions(?string $class = null): array
    {
        $options = $this->options;
        $options["role"] = "alert";
        $options["class"] = "alert alert-" . $this->type;
        if ($class) {
            $options["class"] .= " " .$class;
        }
        return $options;
    }


    public function toString(array $additional = []): string
    {
        return $this->content;
    }
}