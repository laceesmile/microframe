<?php

declare(strict_types=1);

namespace Microframe\Odm;

use Microframe\Database\ManagerInterface;
use Microframe\Database\AbstractConnection;
use Microframe\Odm\Mongo\Manager as MongoManager;
use Microframe\Database\Exception\ConnectionException;

class Connection extends AbstractConnection
{
    const ODM_CONNECTION_NAME = "nosql";

    static array $managers = [
        "mongo" => MongoManager::class,
    ];

    public function getManager(string $driverName): ManagerInterface
    {
        $driver = $this->getDriver($driverName);
        if (!is_null($driver)) {
            return new $driver();
        }
        throw new ConnectionException($this->driver);
    }

    public function getName(): string
    {
        return self::ODM_CONNECTION_NAME;
    }

    public function getConnection()
    {
        return $this->connection;
    }

    private function getDriver(string $driverName): ?string
    {
        foreach (static::$managers as $manager) {
            $supportedDrivers = $manager::getSupportedDrivers();
            if (in_array($driverName, $supportedDrivers)) {
                return $manager;
            }
        }
        return null;
    }
}