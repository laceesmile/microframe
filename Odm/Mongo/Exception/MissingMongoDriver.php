<?php

declare(strict_types=1);

namespace Microframe\Odm\Mongo\Exception;

use Microframe\Common\Exception\MicroframeException;

class MissingMongoDriver extends MicroframeException
{
    public function __construct(int $code = 500)
    {
        $this->code = $code;
        $this->message = "ext-mongodb not installed on your server! Please install and add it to your php.ini.";
    }
}