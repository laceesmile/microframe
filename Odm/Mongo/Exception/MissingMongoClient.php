<?php

declare(strict_types=1);

namespace Microframe\Odm\Mongo\Exception;

use Microframe\Common\Exception\MicroframeException;

class MissingMongoClient extends MicroframeException
{
    public function __construct(int $code = 500)
    {
        $this->code = $code;
        $this->message = "Mongodb/Client not found! You can install it by 'composer require mongodb/mongodb' command.";
    }
}