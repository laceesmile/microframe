<?php

declare(strict_types=1);

namespace Microframe\Odm\Mongo;

use MongoDB\Client;
use Microframe\Database\AbstractDatabaseManager;
use Microframe\Database\DatabaseManagerInterface;
use Microframe\Odm\Mongo\Exception\MissingMongoClient;
use Microframe\Odm\Mongo\Exception\MissingMongoDriver;

class DatabaseManager extends AbstractDatabaseManager implements DatabaseManagerInterface
{
    const SUPPORTED_DRIVERS = [
        "mongo",
        "mongodb"
    ];

    public function getDriverDependencies(): array
    {
        return [
            Client::class => MissingMongoClient::class,
        ];
    }

    public function getExtensionDependencies(): array
    {
        return [
            "mongodb" => MissingMongoDriver::class,
        ];
    }

    public static function getSupportedDrivers(): array
    {
        return self::SUPPORTED_DRIVERS;
    }
}