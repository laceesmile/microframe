<?php

declare(strict_types=1);

namespace Microframe\Odm\Mongo;

use MongoDB\Client;
use Microframe\Database\ManagerInterface;
use Microframe\Odm\Mongo\DatabaseManager;

/**
 * MongoDB NoSql connection.
 * Supported only one connection.
 */

class Manager extends DatabaseManager implements ManagerInterface
{
    const DEFAULT_PORT = 27017;
    const DEFAULT_HOST = "localhost";
    const MONGO_BASE_URI = "mongodb://";

    private $connection;

    public function buildConnection(array $connectionParams)
    {
        $database = $connectionParams["database"];
        $uri = $this->buildUri($connectionParams);

        $this->checkDependencies();

        $this->connection = (new Client($uri))->$database;
    }

    /**
     * Get the connection
     *
     * @param string $name
     * @return void
     */
    public function getConnection(string $name = null)
    {
        return $this->connection;
    }

    private function buildUri(array $connectionParams): string
    {
        $host = $connectionParams["host"] ?? self::DEFAULT_HOST;
        $username = $connectionParams["username"] ?? null;
        $password = $connectionParams["password"] ?? null;
       
        $port = $connectionParams["port"] ?? self::DEFAULT_PORT;

        $uri = self::MONGO_BASE_URI;

        if ($username) {
            $uri .= $username;
            if ($password) {
                $uri .= ":" . $password;
            }
            $uri .= "@";
        }
        $uri .= $host . ":" . $port;
        return $uri;
    }

    public function close()
    {
        $this->connection;
        return;
    }
}