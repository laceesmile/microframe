<?php

declare(strict_types=1);

namespace Microframe\Http\Response;

use Microframe\Http\Response\ResponseInterface;

abstract class AbstractResponse implements ResponseInterface
{
    
}