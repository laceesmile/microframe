<?php

declare(strict_types=1);

namespace Microframe\Http\Response\Exception;

use Microframe\Common\Exception\MicroframeException;

class RequiredParameterException extends MicroframeException
{
    public function __construct(string $parameter, int $code = 404)
    {
        $this->code = $code;
        $this->message = "Parameter {$parameter} required.";
    }
}