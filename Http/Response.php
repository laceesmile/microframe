<?php

declare(strict_types=1);

namespace Microframe\Http;

use InvalidArgumentException;
use JsonSerializable;
use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Helper\Helper;
use Microframe\Http\Response\Exception\RequiredParameterException;
use Microframe\Http\Url;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Mvc\View;
use Microframe\Router\Router;

class Response implements RegistableInterface
{
    use Helper, RegisterTrait;

    private array $header = [];
    private bool $isJson = false;
    private ?string $content = null;
    private bool $redirection = false;

    private ?Router $router = null;
    private ?View $view = null;
    private const MESSAGE_KEY = "message";
    private const CODE_KEY = "code";

    public function redirectRaw(Url $location, array $parameters = [], int $statusCode = 302): self
    {
        $this->addHeader("Location", $location->get($parameters), $statusCode);
        $this->redirection = true;
        return $this;
    }

    public function redirect(array $location, array $parameters = [], int $statusCode = 302): self
    {
        if (!isset($location["for"])) {
            throw new RequiredParameterException("for");
        }
        $this->bootServices();
        $route = $this->router->getRouteByName($location["for"]);
        unset($location["for"]);
        $this->redirectRaw($route->getUrl(), array_merge($location, $parameters), $statusCode);
        return $this;
    }

    public function errorPage()
    {
        $this->bootServices();
        $route = $this->router->getErrorPage();
        $this->redirectRaw($route->getUrl());
        return $this;
    }

    public function redirectNow(array $location, array $parameters = [], int $statusCode = 302): void
    {
        $this->redirect($location, $parameters, $statusCode);
        $this->send();
        exit(1);
    }

    public function redirectRawNow(Url $location, array $parameters = [], int $statusCode = 302): void
    {
        $this->redirectRaw($location, $parameters, $statusCode);
        $this->send();
        exit(1);
    }

    public function externalRedirect(string $url): void
    {
        $this->redirection = true;
        
        $this->addHeader("Location", $url);
    }

    public function json($content): self
    {
        if ($content === null) {
            $this->isJson = true;
            return $this;
        }
        if (!is_array($content) and !($content instanceof JsonSerializable) and !method_exists($content, "toArray")) {
            throw new InvalidArgumentException("JSON response must be array or JsonSerializable.", 500);
        }
        $content = $this->getArray($content);
        $this->addHeader("Content-Type", "application/json");
        $this->isJson = true;
        if (isset($content[self::CODE_KEY])) {
            $this->setResponseCode($content[self::CODE_KEY]);
        }
        $this->content = json_encode($content);
        return $this;
    }

    public function html(): self
    {
        if (empty($this->getHeaders("Content-Type"))) {
            $this->addHeader("Content-Type", "text/html");
        }
        $this->bootServices();
        $this->content = $this->view->build();
        return $this;
    }

    public function send($response = null): void
    {
        if ((!$this->getDI()->has('view')) or ($response and (is_array($response) or method_exists($response, "toArray")))) {
            $this->json($response);
        }
        if (!$this->redirection and !$this->isJson) {
            $this->html();
        }
        foreach ($this->getHeaders() as $type => $value) {
            header($type.": ". $value);
        }
        echo $this->content;
    }

    public function sendException(\Throwable $e)
    {
        $this->setResponseCode($e->getCode());
        $this->send([self::MESSAGE_KEY => $e->getMessage(), self::CODE_KEY => $e->getCode()]);
    }

    public function sendNow($response = null): void
    {
        $this->send($response);
        exit(1);
    }

    private function bootServices(): void
    {
        if (!$this->view or !$this->router) {
            $di = new DI();
            $this->router = $di->get(Router::getRegisterName());
            $this->view = $di->get(View::getRegisterName());
        }
    } 

    public function getHeaders(): array
    {
        return $this->header;
    }

    public function getHeader(string $key): ?string
    {
        return $this->header[$key] ?? null;
    }

    public function addHeader(string $key, string $value): self
    {
        $this->header[$key] = $value;

        return $this;
    }

    public function emptyHeaders(): self
    {
        header_remove();

        return $this;
    }

    public function setResponseCode(int $code): self
    {
        http_response_code($code);

        return $this;
    }
}