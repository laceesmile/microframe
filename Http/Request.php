<?php

declare(strict_types=1);

namespace Microframe\Http;

use Microframe\Common\RegisterTrait;
use Microframe\Http\Url;
use Microframe\Router\Route;
use Microframe\Http\Request\AbstractRequest;
use Microframe\Http\Request\Exception\PhpExtensionNotFound;
use Microframe\Interfaces\RegistableInterface;

class Request extends AbstractRequest implements RegistableInterface
{
    use RegisterTrait;
    
    protected string $tld;
    protected string $domain;
    protected $subdomain;
    protected Url $url;
    protected array $routeParams = [];

    public function __construct()
    {
        $this->serverParams = $_SERVER;
        $this->parse();
        $this->requestArray = $this->parseRequest();
        $this->handleUploadedFiles();
    }

    public function parse(): self
    {
        $this->requestMethod = $this->serverParams['REQUEST_METHOD'] ?? "GET";

        $this->url = new Url($_REQUEST["url"] ?? self::URL_DELIMITER);

        if (php_sapi_name() !== "cli") {
            $this->setupSubdomainParts();
        }

        $this->headers = $this->getAllHeaders();

        return $this;
    }

    public function setRouteParam(string $key, mixed $value): self
    {
        $this->routeParams[$key] = $value;

        return $this;
    }

    public function setRouteParams(array $params): self
    {
        $this->routeParams = array_merge($this->routeParams, $params);

        return $this;
    }

    public function getRouteParam(string $key): mixed
    {
        return $this->routeParams[$key] ?? null;
    }

    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function hasRouteParam(string $key): bool
    {
        return isset($this->routeParams[$key]);
    }

    private function setupSubdomainParts()
    {
        $host = $this->getServer("HTTP_HOST");
        $hostArray = explode(".", $host);

        $this->tld = array_pop($hostArray);
        $this->domain = array_pop($hostArray);

        if (count($hostArray) > 1) {
            $this->subDomain = $hostArray;
        } else {
            $this->subDomain = array_shift($hostArray);
        }
    }

    public final function has(string $name): bool
    {
        return isset($this->requestArray[$name]);
    }

    public function getBasicAuth(): ?array
    {
        if ($this->hasServer("PHP_AUTH_USER") or $this->hasServer("PHP_AUTH_PW")) {
            return null;
        }
    
        return [
            "username" => $this->getServer("PHP_AUTH_USER"),
            "password" => $this->getServer("PHP_AUTH_PW")
        ];
    }

    public function getScheme(): ?string
    {
        return $this->getServer("REQUEST_SCHEME");
    }

    public function getBestLanguage(bool $shortOnly = false): ?string
    {
        if (!$this->hasHeader("Accept-Language")) {
            return null;
        }
        if (!extension_loaded("intl")) {
            throw new PhpExtensionNotFound("intl" ,  "This will help to decide the client default language");
        }
        $locale = \Locale::acceptFromHttp ($this->getHeader("Accept-Language"));
        return $shortOnly ? substr($locale, 0, 2) : $locale;
    }

    public function getHeader(string $name): ?string
    {
        if ($name == null) {
            return $this->headers ?? [];
        }
        return $this->headers[$name] ?? null;
    }

    public function getHeaders(): array
    {
        return $this->headers ?? [];
    }

    public function hasHeader(string $headerName): bool
    {
        return isset($this->headers[$headerName]);
    }

    /**
     * Get the value of url
     */ 
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the value of requestMethod
     */ 
    public function getMethod()
    {
        return $this->requestMethod;
    }

    /**
     * Get the value of matchedRoute
     * 
     * @return Route
     */ 
    public function getMatchedRoute(): ?Route
    {
        return $this->matchedRoute;
    }

    /**
     * Set the value of matchedRoute
     *
     * @param Route $matchedRoute
     * 
     * @return Request
     */ 
    public function setMatchedRoute(Route $matchedRoute): self
    {
        $this->matchedRoute = $matchedRoute;

        return $this;
    }

    /**
     * Get the value of tld
     */ 
    public function getTld()
    {
        return $this->tld;
    }

    /**
     * Get the value of domain
     */ 
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get the value of subDomain
     * @return array|string|null
     * null when no subdomain
     * string when one subdomain
     * array gives back all subdomain like: dev.project1.preview.domain.com => ["dev", "project1", "preview"]
     */ 
    public function getSubDomain()
    {
        return $this->subDomain;
    }
}