<?php

declare(strict_types=1);

namespace Microframe\Http;

use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Http\Url\Exception\NotFilledUrlParamsException;
use Microframe\Interfaces\RegistableInterface;

class Url implements RegistableInterface
{
    use RegisterTrait;

    /**
     * default URL variable match type
     */
    private const DEFAULT_MATCH_TYPE = "string";

    /**
     * Key in url to use all remaining parameters as variable
     */
    public const URL_ACCEPT_ALL = "params";

    /**
     * Regex patterns to types and URL_ACCEPT_ALL
     */
    private const REGEX_URL_TYPE = [
        "int" => "(\d+)",
        "string" => "([^/]*)",
        self::URL_ACCEPT_ALL => ".*",
    ];

    public string $url = "/";
    public array $replacable = [];
    public string $regex;
    public array $replaceType = [];

   public function __construct(string $url = null)
   {
        if (!is_null($url)) {
            $this->setUrl($url);
        }
   }

   /**
    * Generate URL from array by name
    *
    * @param array $parameters
    * @return string
    */
    
    public function get(array $parameters = []): string
    {
        if (isset($parameters["for"])) {
            $url = clone $this->prepareUrl($parameters);
        } else {
            $url = clone $this;
            if (preg_match_all('/:(.*?)\:/', $this->getUrl(), $matches)) {
                $url->replacable = $matches[1];
            }
        }
        $link = $url->getUrl();
        $get = [];
        foreach ($parameters as $key => $value) {
            $value = urlencode((string) $value);
            if (in_array($key, $url->replacable)) {
                $link = preg_replace("/:$key:/", "$value", $link);
                $replacableIndex = array_search($key, $url->replacable);
                unset($url->replacable[$replacableIndex]);
            } else {
                $get[$key] = $value;
            }
        }
        if (!empty($url->replacable)) {
            throw new NotFilledUrlParamsException($url->replacable);
        }
        if (!empty($get)) {
            $link .= '?' . http_build_query($get);
        }
        return $link;
    }
    
    public function getFullUrl(array $parameters = [])
    {
        $request = (new DI())->get("request");
        if (isset($parameters["baseUri"])) {
            $baseUrl = $request->getScheme() . "://" . $parameters["baseUri"];
            unset($parameters["baseUri"]);
        } else {
            $baseUrl = $request->getScheme() . "://" . $request->getServer("SERVER_NAME");
        }
        return $baseUrl . $this->get($parameters);
    }

    private function prepareUrl(array &$parameters)
    {
        $router = (new DI())->get("router");
        $routeName = $parameters["for"];
        unset($parameters["for"]);
        return $router->getRouteByName($routeName)->getUrl();
    }

    public function addReplacable(string $replace)
    {
        $this->replacable[] = $replace;
    }

    public function setReplacable(array $replace)
    {
        $this->replacable = $replace;
    }

    public function addReplaceType(string $replaceKey, string $type)
    {
        $this->replaceType[$replaceKey] = $type;
    }

    /**
     * Get the value of regex
     * 
     * @return string
     */ 
    public function getRegex(): string
    {
        return $this->regex;
    }

    /**
     * Set the value of regex
     *
     * @param string $regex
     * 
     * @return  self
     */ 
    public function setRegex(string $regex): self
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Get the value of url
     * 
     * @return string
     */ 
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * Set the value of url
     * 
     * @param string $url
     *
     * @return  self
     */ 
    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
}