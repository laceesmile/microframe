<?php

declare(strict_types=1);

namespace Microframe\Http\Request\Exception;

use Microframe\Common\Exception\MicroframeException;

class PhpExtensionNotFound extends MicroframeException
{
    public function __construct(string $extensionName, string $message, int $code = 500)
    {
        $this->code = $code;
        $this->message = $extensionName . " PHP extension not found. " . $message;
    }
}