<?php

declare(strict_types=1);

namespace Microframe\Http\Request\Exception;

use Microframe\Common\Exception\MicroframeException;

class UnsupportedRequestTypeException extends MicroframeException
{
    public function __construct(string $type, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Unsupported request type {$type}";
    }
}