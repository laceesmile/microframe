<?php

declare(strict_types=1);

namespace Microframe\Http\Request;

use Microframe\Router\Route;
use Microframe\Helper\Helper;
use Microframe\Http\Request\RequestFile;

abstract class AbstractRequest
{
    use Helper;
    use RequestFile;

    const URL_DELIMITER = "/";

    protected array $requestArray = [];
    protected array $serverParams = [];
    protected string $requestMethod = "POST";
    protected ?Route $matchedRoute = null;

    /**
     * Getters
     */

    public function get(?string $name = null)
    {
        $source = ($this->isGet() and !empty($_GET)) ? $_GET : $this->requestArray;
        if ($name == null) {
            return $source ?? [];
        }
        if (!$this->constValue("MICROFRAME_DISABLE_REQUEST_PARAMS_NAME_CAMELIZE", false)) {
            $name = $this->camelize($name);
        }
        return $source[$name] ?? null;
    }

    public function getCalledUrl(): string
    {
        return $this->getServer("HTTP_REFERER");
    }

    public function isMobile(): bool
    {
        $useragent = $_SERVER['HTTP_USER_AGENT'];

        return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
    }

    private function getRawBody(): string
    {
        return file_get_contents("php://input");
    }

    protected function parseRequest(): array
    {
        unset($_GET["url"]);
        $this->normalizeRequestArray($_GET);
        $contentType = $this->getServer("CONTENT_TYPE");
        if ($contentType and strpos($contentType, "json") > 0) {
            $parsed = $this->getJsonRawBody() ?? $_REQUEST;
        } else {
            parse_str($this->getRawBody(), $parsed);
        }
        unset($parsed["url"]);
        $request = $this->parseParams($parsed ?? []);
        $this->normalizeRequestArray($request);

        return $request;
    }

    private function normalizeRequestArray(array &$requestArray): void
    {
        array_walk_recursive($requestArray, function(&$v) {
            settype($v, "string");
            $v = urldecode(trim($v));
        });
    }

    private function parseParams(array $params): array
    {
        if ($this->constValue("MICROFRAME_DISABLE_REQUEST_PARAMS_NAME_CAMELIZE", false)) {
            return $params;
        }
        return array_combine(array_map([$this, "camelize"], array_keys($params)), array_values($params));
    }

    public function getServer(?string $name = null)
    {
        if ($name == null) {
            return $this->serverParams ?? [];
        }
        return $this->serverParams[$name] ?? null;
    }

    public final function hasServer(string $name): bool
    {
        return isset($this->serverParams[$name]);
    }

    public function getRaw(): string
    {
        return $this->getRawBody();
    }

    public function getJsonRawBody(): ?array
    {
        return json_decode($this->getRawBody(), true);
    }

    public function getPost(): array
    {
        return $this->get();
    }

    public function getGet(): array
    {
        return $this->parseParams($_GET);
    }

    public function getDelete(): array
    {
        return $this->requestArray;
    }

    public function getHead(): array
    {
        return $this->requestArray;
    }

    public function getPatch(): array
    {
        return $this->requestArray;
    }

    public function getUpdate(): array
    {
        return $this->requestArray;
    }

    public function getOptions(): array
    {
        return $this->requestArray;
    }

    public function getPut(): array
    {
        return $this->requestArray;
    }

    /**
     * End of getters
    */

    /**
     * type getters
    */

    public function isAjax(): bool
    {
        return $this->hasServer("HTTP_X_REQUESTED_WITH") and $this->getServer("HTTP_X_REQUESTED_WITH") === "XMLHttpRequest";
    }

    public function is(string $type): bool
    {
        return $this->requestMethod == strtoupper($type);
    }

    public function isPost(): bool
    {
        return $this->is("POST");
    }

    public function isGet(): bool
    {
        return $this->is("GET");
    }

    public function isDelete(): bool
    {
        return $this->is("DELETE");
    }

    public function isHead(): bool
    {
        return $this->is("HEAD");
    }

    public function isPatch(): bool
    {
        return $this->is("PATCH");
    }

    public function isUpdate(): bool
    {
        return $this->is("UPDATE");
    }

    public function isOptions(): bool
    {
        return $this->is("OPTIONS");
    }

    public function isPut(): bool
    {
        return $this->is("PUT");
    }

    protected function getAllHeaders(): array
    {
        if (!function_exists('getallheaders')) 
        { 
            $headers = array ();
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
            return $headers;
        } else {
            return getallheaders();
        }
    }
}