<?php

declare(strict_types=1);

namespace Microframe\Http\Request;

use Microframe\File\File;
use Microframe\Common\NativeArray;

trait RequestFile
{
    public NativeArray $fileList;

    public function handleUploadedFiles()
    {
        if (!empty($_FILES)) {
            foreach ($_FILES as $name => $file) {
                if (is_array($file["tmp_name"])) {
                    foreach ($file["tmp_name"] as $key => $value) {
                        if ($file["error"][$key] !== 0) {
                            continue;
                        }
                        $fileArray = [
                            "tmp_name" => $value,
                            "name" => $file["name"][$key],
                        ];
                        $this->fileList[$this->camelize($name)][] = (new File())->buildFileFromRequest($fileArray);
                    }
                } else {
                    if ($file["error"] !== 0) {
                        continue;
                    }
                    $this->fileList[$this->camelize($name)] = (new File())->buildFileFromRequest($file);
                }
            }
            if (!empty($this->fileList)) {
                $this->fileList = new NativeArray($this->fileList);
            }
        }
    }

    public function removeFile(string $removeThis): void
    {
        foreach ($this->fileList as $name => $file) {
            if (is_array($file)) {
                foreach ($file as $key => $fileObject) {
                    if ($fileObject->getName() == $removeThis) {
                        unset($this->fileList[$name][$key]);
                    }
                }
            } else {
                unset($this->fileList[$name]);
            }
        }
    }

    public function hasFile(): bool
    {
        return !empty($this->fileList);
    }

    public function getFile(?string $name = null)
    {
        if (isset($this->fileList[$name])) {
            return $this->fileList[$name];
        }
        return null;
    }

    public function getFiles(): NativeArray
    {
        return $this->fileList;
    }
}
