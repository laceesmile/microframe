<?php

declare(strict_types=1);

namespace Microframe\Http\Url\Exception;

use Microframe\Common\Exception\MicroframeException;

class NotFilledUrlParamsException extends MicroframeException
{
    public function __construct(array $notFilledParams, int $code = 500)
    {
        $plural = count($notFilledParams) > 1 ? "Parameters " : "Parameter ";

        $this->code = $code;
        $this->message = "URL generation failed. " . $plural . "'" . join("', '", $notFilledParams) . "' is not filled";
    }
}