<?php

declare(strict_types=1);

namespace Microframe\Translator\Exception;

use Microframe\Common\Exception\MicroframeException;

class UnsupportedSourceException extends MicroframeException
{
    public function __construct(string $extension)
    {
        $this->message = `$extension is not supported bu the framework`;
        $this->code = 405;
    }
}