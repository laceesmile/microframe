<?php

declare(strict_types=1);

namespace Microframe\Translator\Sources;

use Microframe\Common\NativeArray;

interface SourceInterface
{
    public function getContent(): NativeArray;
} 