<?php

declare(strict_types=1);

namespace Microframe\Translator\Sources;

use Microframe\Common\NativeArray;
use Microframe\Helper\Helper;
use Microframe\Translator\Sources\SourceInterface;

class Database implements SourceInterface
{
    use Helper;
    private ?NativeArray $content = null;

    private string $modelClass;
    private string $keyColumn;
    private string $languageColumn;
    private array $filter = [];

    public function __construct(string $modelClass, string $keyColumn, string $languageColumn)
    {
        $this->modelClass = $modelClass;
        $this->keyColumn = $keyColumn;
        $this->languageColumn = $languageColumn;
    }

    public function getContent(): NativeArray
    {
        return $this->read()->content;
    }

    public function setFilter(): self
    {
        $this->filter = func_get_args();

        return $this;
    }

    private function read(): self
    {
        if ($this->content === null) {
            $source = $this->modelClass::select($this->keyColumn, $this->languageColumn);
            if (!empty($this->filter)) {
                $source->where(...$this->filter);
            }
            $source = $source->get();
            $this->content = new NativeArray($this->indexTo($source, $this->keyColumn, $this->languageColumn));
        }
        return $this;
    }
}