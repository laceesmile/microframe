<?php

declare(strict_types=1);

namespace Microframe\Translator\Sources;

use Microframe\File\AbstractFile;
use Microframe\Common\NativeArray;
use Microframe\Translator\Sources\SourceInterface;

class PhpArray extends AbstractFile implements SourceInterface
{
    public function __construct($file)
    {
        parent::__construct($file.".php");
    }

    public function read(): self
    {
        if (empty($this->content)) {
            $this->content = new NativeArray(include($this->file));
        }
        return $this;
    }

    public function getContent(): NativeArray
    {
        return $this->read()->content;
    }
}