<?php

declare(strict_types=1);

namespace Microframe\Translator\Sources;

use Microframe\File\Json as FileJson;
use Microframe\Translator\Sources\SourceInterface;

class Json extends FileJson implements SourceInterface
{

}