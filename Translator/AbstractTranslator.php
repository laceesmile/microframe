<?php

declare(strict_types=1);

namespace Microframe\Translator;

use Microframe\Common\NativeArray;
use Microframe\Translator\Exception\UnsupportedSourceException;
use Microframe\Translator\Sources\Csv;
use Microframe\Translator\Sources\Json;
use Microframe\Translator\Sources\PhpArray;
use Microframe\Translator\Sources\SourceInterface;
use Microframe\Translator\TranslatorInterface;

abstract class AbstractTranslator implements TranslatorInterface
{
    protected NativeArray $translations;

    public function __construct(string $sourceFile = null)
    {
        if ($sourceFile and file_exists($sourceFile)) {
            $pathInfo = pathinfo($sourceFile);
            $name = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $pathInfo['filename'];
            $ext = pathinfo($sourceFile)['extension'];
            $this->setSource($this->getFileSource($name, $ext)->getContent());
        }
    }

    public function setSource(NativeArray $array): self
    {
        $this->translations = $array;

        return $this;
    }

    public function _(string $key, $param = null): string
    {
        if ($param == null) {
            return $this->t($key);
        } elseif (is_array($param)) {
            return $this->ta($key, $param);
        } else {
            return $this->tp($key, $param);
        }
    }

    private function getFileSource(string $file, string $extension): SourceInterface
    {
        return match($extension) {
            'json' => new Json($file),
            'php' => new PhpArray($file),
            'csv' => new Csv($file),
            default => throw new UnsupportedSourceException($extension)
        };
    }
}