<?php

declare(strict_types=1);

namespace Microframe\Translator;

use Microframe\Common\RegisterTrait;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Translator\AbstractTranslator;

class Translator extends AbstractTranslator implements RegistableInterface
{
    use RegisterTrait;
    
    private string $language = "en";

    public function t(string $translateKey): string
    {
        return $this->translations[$translateKey] ?? $translateKey;
    }

    public function ta(string $translateKey, array $array): string
    {
        $translated = $this->t($translateKey);
        foreach ($array as $key => $value) {
            $translated = preg_replace('(%\b'.$key.'*%)', (string) $value, $translated);
        }
        return $translated;
    }

    public function tp(string $translateKey, ?string $placeholder = null): string
    {
        return preg_replace('(%\S*%)', $placeholder, $this->t($translateKey));
    }

    /**
     * Get the value of language
     */ 
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set the value of language
     *
     * @return  self
     */ 
    public function setLanguage(string $language)
    {
        $this->language = $language;

        return $this;
    }
}