<?php

declare(strict_types=1);

namespace Microframe\Translator;

use Microframe\Common\NativeArray;

interface TranslatorInterface
{
    public function setSource(NativeArray $array): self;

    public function t(string $translateKey): string;

    public function ta(string $translateKey, array $array): string;

    public function tp(string $translateKey, ?string $placeholder = null): string;

    public function _(string $key, $param = null): string;
}