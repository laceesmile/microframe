<?php

declare(strict_types=1);

namespace Microframe\Router;

use Microframe\Http\Url;
use Microframe\Router\Route;

class RouteNotFoundRoute extends Route
{
    public function __construct(?string $baseNamespace)
    {
        parent::__construct(new Url("/microframe/error"), [
            "controller" => __NAMESPACE__ . "\\ErrorPageController",
            "action" => "error",
            "basenamespace" => $baseNamespace,
        ]);
    }
}

class ErrorPageController
{
    public function errorAction()
    {
        var_dump($_SERVER);
    }
}