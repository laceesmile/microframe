<?php

declare(strict_types=1);

namespace Microframe\Router;

use Microframe\Http\Url;
use Microframe\Router\Route;

abstract class AbstractRouter
{
    /**
     * Add a route definition
     *
     * @param Url $url
     * @param array $routerParams contains router definition
     * @param string|null $module
     * @return Route
     */
    abstract public function addRoute(string $url, array $routerParams = [], ?string $module = null): Route;

    /**
     * Add an ANY type route
     */
    public function add(string $url, array $routerParams, ?string $module = null): Route
    {
        return $this->addRoute($url, $routerParams, $module);
    }
    
    /**
     * Add a GET type route
     */
    public function addGet(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "get";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add a POST type route
     */
    public function addPost(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "post";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add a PUT type route
     */
    public function addPut(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "put";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add a HEAD type route
     */
    public function addHead(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "head";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add a DELETE type route
     */
    public function addDelete(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "delete";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add a PATCH type route
     */
    public function addPatch(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "patch";
        return $this->addRoute($url, $routerParams, $module);
    }

    /**
     * Add an OPTIONS type route
     */
    public function addOptions(string $url, array $routerParams, ?string $module = null): Route
    {
        $routerParams["method"] = "options";
        return $this->addRoute($url, $routerParams, $module);
    }
}