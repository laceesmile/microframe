<?php

declare(strict_types=1);

namespace Microframe\Router;

use Microframe\Application\Autowire;
use Microframe\Application\Dispatcher;
use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Helper\Helper;
use Microframe\Http\Request;
use Microframe\Http\Url;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Router\AbstractRouter;
use Microframe\Router\Exception\BadRouterConfigurationException;
use Microframe\Router\Exception\RouteNotFoundException;
use Microframe\Router\Exception\RouteParameterMissmatchException;
use Microframe\Router\Route;
use Microframe\Router\RouteNotFoundRoute;
use Microframe\Router\RoutesInterface;

class Router extends AbstractRouter implements RegistableInterface
{
    use Helper, RegisterTrait;

    /**
     * Key in url to use all remaining parameters as variable
     */
    public const URL_ACCEPT_ALL = "params";

    /**
     * key used to unidentified routes
     * where used add(...) function
     */
    public const UNKNOW_METHOD_KEY = "any";

    /**
     * Accept header key
     */
    public const CONTENT_TYPE_HEADER = "Content-Type";

    /**
     * Errorpage definition route
     *
     * @var RouteNotFoundRoute
     */
    public RouteNotFoundRoute $errorPage;

    /**
     * Default module name
     *
     * @var string
     */
    public string $defaultModule;

    /**
     * Base namespace
     * append before every controller namespace
     * use without slash
     *
     * @var string
     */
    public ?string $baseNamespace;

    /**
     * Routes container indexet to name
     *
     * @var array
     */
    private array $routesByName = [];

    /**
     * Routes container
     *
     * @var array
     */
    private array $routes = [];

    /**
     * Regex patterns to types and URL_ACCEPT_ALL
     */
    private const REGEX_URL_TYPE = [
        "int" => "(\d+)",
        "string" => "([^/]*)",
        self::URL_ACCEPT_ALL => ".*",
    ];

    /**
     * controller key in url definition
     */
    private const ROUTE_CONTROLLER_NAME_INDEX = "controller";

    /**
     * controller folder name
     */
    private const CONTROLLER_FOLDER_NAME = "controller";

    /**
     * controller filename suffix
     */
    private const CONTROLLER_POST_NAMESPACE = "Controller";

    /**
     * namespace delimiter
     */
    private const NAMESPACE_DELIMITER = "\\";

    /**
     * default URL variable match type
     */
    private const DEFAULT_MATCH_TYPE = "string";

    /**
     * Autowire service
     * 
     * @var Autorwire
     */
    private Autowire $autowire;

    /**
     * Initiate a router
     *
     * @param string $baseNamespace The base namespace, prepend every route
     */
    public function __construct(string $baseNamespace = null)
    {
        $this->baseNamespace = $baseNamespace;
        $this->errorPage = new RouteNotFoundRoute($this->baseNamespace);
        $this->autowire = (new DI())->get("autowire");
    }

    /**
     * Add routes using object
     *
     * @param RoutesInterface $routes
     * @param string $module
     * @return void
     */
    public function addRoutes(RoutesInterface $routes, string $module)
    {
        $routes->initialize($this, $module);
    }

    /**
     * List of router namespaces
     *
     * @param array $routerArray
     * @return void
     */
    public function parseRoutes(array $routerArray)
    {
        foreach ($routerArray as $namespace) {
            $this->parseRoute($namespace);
        }
    }

    public function parseRoute(string $namespace)
    {
        $module = $this->getModuleFromNamespace($namespace);
        $route = new $namespace();
        $this->addRoutes($route, $module);
    }

    private function generateControllerNamespace(string $controller, string $module = null): string
    {
        return implode(self::NAMESPACE_DELIMITER, array_filter([
            $this->getBaseNamespace($module),
            ucfirst(self::CONTROLLER_FOLDER_NAME),
            ucfirst($controller) . self::CONTROLLER_POST_NAMESPACE
        ]));
    }

    private function getBaseNamespace(string $module = null): string
    {
        return join(self::NAMESPACE_DELIMITER, array_map("ucfirst", array_filter([
            $this->baseNamespace,
            $module ?? $this->getDefaultModule(),
        ])));
    }

    private function getModuleFromNamespace(string $namespace): string
    {
        $namespaceParts = explode(self::NAMESPACE_DELIMITER, $namespace);
        if ($namespaceParts[0] === $this->baseNamespace) {
            array_shift($namespaceParts);
        }
        array_pop($namespaceParts);
        return join(self::NAMESPACE_DELIMITER, $namespaceParts);
    }
    
    /**
     * Add a route with definition
     *
     * @param string $url - example: /index
     * @param array $route
     * @param string|null $module
     * @return Route
     * 
     * @throws BadRouterConfigurationException
     */
    public function addRoute(string $url, array $route = [], ?string $module = null): Route
    {
        if (!isset($route[self::ROUTE_CONTROLLER_NAME_INDEX])) {
            throw new BadRouterConfigurationException($url, self::ROUTE_CONTROLLER_NAME_INDEX);
        }
        $module = $route["module"] ?? $module ?? $this->getDefaultModule();
        $controllerAlias =  $route[self::ROUTE_CONTROLLER_NAME_INDEX];
        $route[self::ROUTE_CONTROLLER_NAME_INDEX] = $this->generateControllerNamespace($controllerAlias, $module);
        $route["controllerAlias"] = $controllerAlias;
        $route["baseNamespace"] = $this->getBaseNamespace($module);

        $url = new Url($url);
        $this->parseUrl($url, $route);
        $urlKey = $route["regex"];
        $method = $route["method"] ?? self::UNKNOW_METHOD_KEY;
        
        if (!isset($route["name"])) {
            $urlNamePart = array_filter([$module, $this->unCamelize($controllerAlias), $this->unCamelize($route["action"])]);
            if ($method !== self::UNKNOW_METHOD_KEY) {
                $urlNamePart[] = $method;
            }
            $routeName = join("_", $urlNamePart);
            $route["name"] = $routeName;
        }

        $this->routes[$method][$urlKey] = new Route($url, $route);
        $this->routesByName[$route["name"]] = $this->routes[$method][$urlKey];
        return $this->routes[$method][$urlKey];
    }

    /**
     * Get the route by request
     * 
     * @param Request $request
     * @return Route
     * 
     * @throws TypeError
     */
    public function getRoute(Request $request): Route
    {
        $acceptHeader = $request->getHeader(self::CONTENT_TYPE_HEADER) == "*/*" ? null : $request->getHeader(self::CONTENT_TYPE_HEADER);
        $url = $request->getUrl() ?? new Url(Request::URL_DELIMITER);

        $possibleRequest = [self::UNKNOW_METHOD_KEY];
        if (isset($this->routes[strtolower($request->getMethod())])) {
            array_unshift($possibleRequest, strtolower($request->getMethod()));
        }
        foreach ($possibleRequest as $requestMethod) {
            if ($route = $this->getRouteByUrl($url->getUrl(), $requestMethod, $request->get(), $acceptHeader)) {
                return $route;
            }
        }
        throw new RouteNotFoundException($url->getUrl());
    }

    /**
     * Get route by URL and method type
     *
     * @param string $url
     * @param string $method
     * @return Route|null
     */
    public function getRouteByUrl(string $url, string $method = "GET", array $requestParams = [], ?string $accept = null): ?Route
    {
        $method = strtolower($method);
        foreach ($this->routes[$method] ?? [] as $regex => $route) {
            preg_match($regex, $url, $matches);
            if (empty($matches)) {
                continue;
            }
            if (!empty($route->getConsume()) and !in_array($accept, $route->getConsume())) {
                continue;
            }
            if (($route->getUrl()->replacable[0] ?? null) === self::URL_ACCEPT_ALL) {
                $matches = preg_split(
                    "/\//",
                    substr(
                        $matches[0],
                        strpos(
                            $route->getUrl()->getUrl(),
                            self::URL_ACCEPT_ALL) - 2,
                            strlen($matches[0]) + 2
                        ),
                    0,
                    PREG_SPLIT_NO_EMPTY
                );
                $requestParams = $this->autowire->wireMethod($route->getController(), $route->getAction() . Dispatcher::ACTION_SUFFIX);
                if (count($requestParams) !== count($matches)) {
                    throw new RouteParameterMissmatchException($route->getUrl()->getUrl(), count($matches), count($requestParams));
                }
            } else {
                unset($matches[0]);
                $matchesAssoc = array_combine($route->getUrl()->replacable, $matches);
            }
            $this->getDI()->request->setRouteParams($matchesAssoc);
            $requestParams = $this->autowire->wireMethod($route->getController(), $route->getAction() . Dispatcher::ACTION_SUFFIX, array_merge($requestParams, $matchesAssoc));
            $route->setParams($requestParams);
            return $route;
        }
        return null;
    }


    /**
     * Extends URL parameter with variables and regex pattern
     *
     * @param Url $url
     * @param array $route
     * @return void
     */
    private function parseUrl(Url $url, array &$route): void
    {
        $regexUrl = [];
        $startWith = "~^" . Request::URL_DELIMITER;
        $endWith = "$~";

        $urlParams = preg_split("/\//", $url->getUrl(), 0, PREG_SPLIT_NO_EMPTY);

        foreach ($urlParams as $keyWord) {
            if (preg_match('/:(.*?)\:/', $keyWord, $matches)) {
                if ($matches[1] === self::URL_ACCEPT_ALL) {
                    $matchType = self::URL_ACCEPT_ALL;
                } else {
                    $matchType = isset($route[$matches[1]]) ? $matchType = $route[$matches[1]] : self::DEFAULT_MATCH_TYPE;
                }
                $route["bindParams"][] = $matches[1];
                $url->addReplacable($matches[1]);
                $regexUrl[] = self::REGEX_URL_TYPE[$matchType];
                $url->addReplaceType($matches[1], $matchType);
            } else {
                $regexUrl[] = $keyWord;
            }
        }
        $regex = $startWith . implode(Request::URL_DELIMITER, $regexUrl) . $endWith;
        $route["regex"] = $regex;
    
        $url->setRegex($regex);
    }

    /**
     * Get a route by name
     *
     * @param string $name
     * @return Route
     * 
     * @throws RouteNotFoundException
     */
    public function getRouteByName(string $name): Route
    {
        if (isset($this->routesByName[$name])) {
            return $this->routesByName[$name];
        }
        throw new RouteNotFoundException($name);
    }

    /**
     * Get the value of errorPage
     *
     * @return RouteNotFoundRoute
     */
    public function getErrorPage(): RouteNotFoundRoute
    {
        return $this->errorPage;
    }

    /**
     * Add errorpage definition
     *
     * @param string $url - which appears in browser
     * @param array $routeDefinition - 
     * [
     * "controller" => "controllerAlias",
     *  "action" => "errorAction"
     * ]
     * @param string|null $module
     * @return self
     */
    public function setErrorPage(string $url, array $routeDefinition, ?string $module = null): self
    {
        
        $this->errorPage = $this->addGet($url, $routeDefinition, $module);

        return $this;
    }

    /**
     * Set the error page by name
     *
     * @param string $name - The route name
     * @return self
     */
    public function setErrorPageByName(string $name): self
    {
        $this->errorPage = $this->getRouteByName($name);

        return $this;
    }

    /**
     * Get the value of defaultModule
     *
     * @return string|null
     *
     */ 
    public function getDefaultModule(): ?string
    {
        return $this->defaultModule ?? null;
    }

    /**
     * Set the value of defaultModule
     *
     * @return  self
     */ 
    public function setDefaultModule(string $defaultModule): self
    {
        $this->defaultModule = $defaultModule;

        return $this;
    }

    /**
     * Set the value of baseNamespace.
     * Use it without the slashes.
     *
     * @return  self
     */ 
    public function setBaseNamespace(string $baseNamespace): self
    {
        $this->baseNamespace = $baseNamespace;

        return $this;
    }
}