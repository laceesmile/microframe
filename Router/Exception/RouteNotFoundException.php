<?php

declare(strict_types=1);

namespace Microframe\Router\Exception;

use Microframe\Common\Exception\MicroframeException;

class RouteNotFoundException extends MicroframeException
{
    public function __construct(string $url, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Route " . $url . " not found!";
    }
}