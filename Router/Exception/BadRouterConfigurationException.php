<?php

declare(strict_types=1);

namespace Microframe\Router\Exception;

use Microframe\Common\Exception\MicroframeException;

class BadRouterConfigurationException extends MicroframeException
{
    public function __construct(string $url, string $missingParam = null, int $code = 500)
    {
        $message = "Bad router configuration: " . $url . ". ";
        if ($missingParam) {
            $message .= "Missing parameter: " . $missingParam;
        }
        $this->code = $code;
        $this->message = $message;
    }
}