<?php

declare(strict_types=1);

namespace Microframe\Router\Exception;

use Microframe\Common\Exception\MicroframeException;

class RouteParameterMissmatchException extends MicroframeException
{
    public function __construct(string $url, int $routeParamNumber, int $actionParamNumber, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Route " . $url . " contains " . $routeParamNumber . " but action accepts ONLY " .  $actionParamNumber. " parameters!";
    }
}