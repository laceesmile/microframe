<?php

declare(strict_types=1);

namespace Microframe\Router;

use Microframe\Helper\Helper;
use Microframe\Http\Url;
use Stringable;

class Route implements Stringable
{
    use Helper;

    private string $method;
    private Url $url;
    private string $regex;
    private string $controller;
    private string $action;
    private array $params = [];
    private string $name;
    private ?string $baseNamespace;
    private array $bindParams = [];
    private string $module;
    private string $controllerAlias;
    private array $consume = [];

/**
 * Initialize an Route with \Microframe\Http\Url
 * 
 *
 * @param Url $url
 * @param array $route use ONLY values which has setter
 */
    public function __construct(Url $url, array $route = [])
    {
        $this->url = $url;
        foreach ($route as $key => $value) {
            $func = "set".ucfirst($key);
            if (method_exists($this, $func)) {
                $this->$func($value);
            }
        }
    }

    /**
     * Set the value of controller
     *
     * @return  self
     */ 
    private function setController(string $controller): self
    {
        $this->controller = ucfirst($this->camelize($controller));

        return $this;
    }

    /**
     * Set the value of params
     *
     * @return  self
     */ 
    public function setParams($params): self
    {
        $replaceTypes = $this->url->replaceType;

        foreach ($replaceTypes as $varName => $type) {
            if ($type !== Router::URL_ACCEPT_ALL) {
                settype($params[$varName], $type ?? "string");
            }
        }
        $this->params = array_filter((array) $params);

        return $this;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }


    /**
     * Get the value of url
     */ 
    public function getUrl(): Url
    {
        return $this->url;
    }

    /**
     * Get the value of controller
     */ 
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * Get the value of function
     */ 
    public function getFunction(): string
    {
        return $this->function;
    }

    /**
     * Get the value of params
     */ 
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Get the value of params
     */ 
    public function getParam(string $name)
    {
        return $this->params[$name] ?? null;
       
    }

    /**
     * Get the value of name
     */ 
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of regex
     */ 
    public function getRegex(): string
    {
        return $this->regex;
    }

    
    /**
     * Get the value of module
     */ 
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * Get the value of controllerAlias
     */ 
    public function getControllerAlias(): string
    {
        return $this->controllerAlias;
    }

    /**
     * Set the value of controllerAlias
     *
     * @return  self
     */ 
    public function setControllerAlias(string $controllerAlias): self
    {
        $this->controllerAlias = ucfirst($this->camelize($controllerAlias));

        return $this;
    }

    /**
     * Get the value of method
     */ 
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get the value of action
     */ 
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Get the value of baseNamespace
     */ 
    public function getBaseNamespace(): ?string
    {
        return $this->baseNamespace;
    }

    /**
     * Get the value of consume
     */ 
    public function getConsume(): array
    {
        return $this->consume;
    }

    /**
     * Get the value of bindParams
     */ 
    public function getBindParams(): array
    {
        return $this->bindParams ?? [];
    }

    /**
     * Set the value of regex
     *
     * @return  self
     */ 
    private function setRegex(string $regex): self
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */ 
    private function setMethod(string $method): self
    {
        $this->method = strtoupper($method);

        return $this;
    }

    /**
     * Set the value of action
     *
     * @return  self
     */ 
    private function setAction(string $action): self
    {
        $this->action = lcfirst($this->camelize($action));

        return $this;
    }

    /**
     * Set the value of baseNamespace
     *
     * @return  self
     */ 
    private function setBaseNamespace(?string $baseNamespace): self
    {
        $this->baseNamespace = $baseNamespace;

        return $this;
    }

    /**
     * Set the value of bindParams
     *
     * @return  self
     */ 
    private function setBindParams(?array $bindParams = []): self
    {
        $this->bindParams = $bindParams;

        return $this;
    }

    /**
     * Set the value of module
     *
     * @return  self
     */ 
    private function setModule(string $module): self
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Set the value of consume
     *
     * @return  self
     */ 
    private function setConsume($consume): self
    {
        $this->consume = (array) $consume;

        return $this;
    }

    public function __toString(): string
    {
        return $this->url->getUrl();
    }
}