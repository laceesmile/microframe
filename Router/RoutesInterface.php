<?php

declare(strict_types=1);

namespace Microframe\Router;

interface RoutesInterface
{
    /**
     * Initialize a Router object
     *
     * @param Router $router
     * @param string $module
     * 
     * @return Router
     */
    public function initialize(Router $router, string $module): Router;
}