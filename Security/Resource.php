<?php

declare(strict_types=1);

namespace Microframe\Security;

class Resource
{
    protected string $module;
    protected string $controller;
    protected string $action;
    protected array $params = [];

    /**
     * Create new resource object
     *
     * @param string $module like "core"
     * @param string $controller like "index"
     * @param string $action like "news"
     */
    public function __construct(string $module, string $controller, string $action)
    {
        $this->module = $module;

        $this->controller = $controller;
        
        $this->action = $action;
    }

    public function addParams(string $key, $value)
    {
        $this->params[$key] = $value;

        return $this;
    }


    /**
     * Get the value of controller
     */ 
    public function getController(): string
    {
        return $this->controller;
    }

    /**
     * Get the value of action
     */ 
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * Get the value of module
     */ 
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Get the value of params
     */ 
    public function getParams(?string $name = null)
    {
        if ($name == null) {
            return $this->params;
        }

        return $this->params[$name];
    }
}