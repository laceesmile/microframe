<?php

declare(strict_types=1);

namespace Microframe\Security;

use Microframe\Router\Route;
use Microframe\Router\Router;
use Microframe\Common\NativeArray;
use Microframe\DI;

class Acl extends DI
{
    /**
     * Layers container
     *
     * @var NativeArray
     */
    private NativeArray $container;

    /**
     * Router to use security
     *
     * @var Router
     */
    private Router $router;

    public function __construct()
    {
        $this->router = $this->get("router");
        $this->container = new NativeArray();
    }

    /**
     * Add a layer to the container
     *
     * @param Layer $layer
     * @return self
     */
    public function addLayer(Layer $layer): self
    {
        $roleName = $layer->getName();
        
        $this->container->$roleName = $layer;

        return $this;
    }

    /**
     * Get a security layer.
     * Initialize a new one if not exists and add it to the container
     *
     * @param string $name role name (e.g.: admin)
     * @return Layer
     */
    public function getLayer(string $name): Layer
    {
        if (!empty($this->container[$name])) {
            $layer = $this->container->$name;
        } else {
            $layer = new Layer($name);
            $this->addLayer($layer);
        }
        return $layer;
    }

    public function isEnabled(string $roleName, Resource $resource): bool
    {
        if (count($this->container) == 0 or $this->container->$roleName === null) {
            return false;
        }
        $controller = $resource->getController();
        $action = $resource->getAction();
        return $this->container->$roleName->exists($controller, $action);
    }

    /**
     * Determine route is enabled for a rolename
     *
     * @param string $roleName like "admin"
     * @param string $routeName like "core_index"
     * @return boolean
     */
    public function isEnabledByName(string $roleName, string $routeName): bool
    {
        $route = $this->router->getRouteByName($routeName);
        $resource = new Resource($route->getModule(), $route->getController(), $route->getAction());
        return $this->isEnabled($roleName, $resource);
    }

    public function isEnabledByRoute(string $roleName, Route $route): bool
    {
        $resource = new Resource($route->getModule(), $route->getController(), $route->getAction());
        return $this->isEnabled($roleName, $resource);
    }

    public function getAccessList(string $roleName): ?Layer
    {
        return $this->container->$roleName;
    }

    public function getRoleNames(): NativeArray
    {
        return new NativeArray(array_keys($this->container->toArray()));
    }
}