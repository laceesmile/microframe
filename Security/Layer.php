<?php

declare(strict_types=1);

namespace Microframe\Security;

use Microframe\Router\Route;
use Microframe\Security\Resource;
use Microframe\Common\NativeArray;

class Layer
{
    private const ROLE_NAME_DELIMITER = "!=!";
    protected string $name;
    protected NativeArray $resources;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->resources = new NativeArray();
    }

    public function addResource(Resource $resource): self
    {
        $controller = $resource->getController();

        $action = $resource->getAction();

        $this->resources[$controller . self::ROLE_NAME_DELIMITER . $action] = $resource;

        return $this;
    }

    public function getResourceByRoute(Route $route): ?Resource
    {
        $controller = $route->getController();
        $action = $route->getAction();

        if (!$this->exists($controller, $action)) {
            return null;
        }
        return $this->resources[$controller . self::ROLE_NAME_DELIMITER . $action];
    } 

    public function exists(string $controllerNamespace, string $action): bool
    {
        $resourceName = $controllerNamespace . self::ROLE_NAME_DELIMITER . $action;
        return isset($this->resources[$resourceName]);
    }

    /**
     * Get the value of name
     */ 
    public function getName()
    {
        return $this->name;
    }
}