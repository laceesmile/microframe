<?php

declare(strict_types=1);

namespace Microframe\Security;

class Security
{
    /**
     * Hash a string
     *
     * @param string $password
     * @param integer $type Hash mode - default: CRYPT_BLOWFISH
     * @return string
     */
    public function hash(string $password, int $type = CRYPT_BLOWFISH): string
    {
        return password_hash($password, $type);
    }

    /**
     * Check hash equals
     *
     * @param string $unEncripted
     * @param string $encripted
     * @return boolean
     */
    public function checkHash(string $unEncripted, string $encripted): bool
    {
        return hash_equals(crypt($unEncripted, $encripted), $encripted);
    }
}

