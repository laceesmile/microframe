<?php

declare(strict_types=1);

namespace Microframe\Security;

use Microframe\Html\Message;
use Microframe\Form\Element\Hidden;


trait CsrfProtection
{
    private string $csrfTokenElementName = "_token";
    private static string $csrfSalt = "CsrfHash";
    protected bool $isSecure = false;

    protected function secureCsrf()
    {
        if ($this->isSecure and $this->request->is($this->getMethod())) {
            if (!$this->checkSecure()) {
                $this->cache->remove(static::class . self::$csrfSalt);
                $this->setUp();
                $this->isValid = false;
                $this->setMessage(new Message("CSRF protection was invalidated your request"));
            }
            $this->setUp();
        }
    }

    public function useCSRF()
    {
        $this->isSecure = true;

        $this->setup();

        return $this;
    }

    private function setup()
    {
        $token = $this->getToken();

        $this->addCsrfTokenElement($token);
    }

    private function checkSecure(): bool
    {
        return $this->request->get($this->csrfTokenElementName) === $this->cache->get(static::class . self::$csrfSalt);
    }   

    private function addCsrfTokenElement(string $value)
    {
        $element = new Hidden($this->csrfTokenElementName);
        $this->elementValues[$this->csrfTokenElementName] = $value;
        $this->add($element);
    }

    private function getToken(): string
    {
        if (!$this->cache->has(static::class . self::$csrfSalt) or strtolower($this->request->getMethod()) !== strtolower($this->getMethod())) {
            $token = $this->generateCsrfToken();
            $this->cache->set(static::class . self::$csrfSalt, $token);
        } else {
            $token = $this->cache->get(static::class . self::$csrfSalt);
        }
        return $token;
    }

    private function generateCsrfToken(): string
    {
        return base64_encode(microtime() . static::class . openssl_random_pseudo_bytes(16));
    }

    public function render(array $additional = []): string
    {
        $responseHtml = parent::render($additional);

        if ($this->isSecure === true) {
            $responseHtml .= $this->getElement($this->csrfTokenElementName)->render();
        }

        return $responseHtml;
    }
}