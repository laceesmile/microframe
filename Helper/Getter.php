<?php

declare(strict_types=1);

namespace Microframe\Helper;

use ReflectionProperty;

trait Getter
{
    public function __get($name)
    {
        $fnc = "get".ucfirst($name);
        if (method_exists($this, $fnc)) {
            return $this->$fnc();
        }
        if (!isset($this->$name)) {
            return null;
        }
        $type = (new ReflectionProperty($this, $name))->getType()->getName();
        settype($this->$name, $type);
        return $this->$name;
    }

    public function __call(string $method, array $params = []): mixed
    {
        if (substr($method, 0, 3) == 'get') {
            $varName = lcfirst(substr($method, 3, strlen($method)));
            return $this->$varName;
        }
    }
}