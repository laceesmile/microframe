<?php

namespace Microframe\Helper;

use Microframe\DI;

trait Helper
{
    public function camelCaseToKebabCase(string $input): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '-$0', $input)), '-');
    }

    public function camelCaseToSnakeCase(string $input): string
    {
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $input)), '_');
    }

    public function getDI()
    {
        return new DI();
    }


    /**
     * convert string to camelCase
     *
     * @param string $input
     * @return string
     * @example 1: UPPER_SNAKE_CASE -> upperSnakeCase
     * @example 2: snake_case -> snakeCase
     * @example 3: kebab-case -> kebabCase
     * @example 4: PascalCase -> pascalCase
     */
    public  function camelize(string $input): string
    {
        return lcfirst(implode('', array_map(function ($part) {
            return ucfirst(strtolower($part));
        }, preg_split('/((?=[A-Z][a-z]))|([-_\s])/', $input, -1, PREG_SPLIT_NO_EMPTY))));
    }


    /**
     * convert a PascalCased or camelCased string to snake_case or kebab-case
     *
     * @param string $input
     * @param string $delimiter
     * @param string|null $patternExtend
     * @return string
     */
    public function unCamelize(string $input, string $delimiter = "_", ?string $patternExtend = "-_\s"): string
    {
        $repIndex = 0;
        preg_match("/[$patternExtend]/", $input, $matches);
        if (count($matches) > 0) {
            $repIndex = 1;
        }
        return strtolower(preg_replace('/(?<!^)[A-Z' . $patternExtend . ']/', "$delimiter$" . $repIndex, $input));
    }

    /**
     * Uncamelize an array keys
     *
     * @param array $inputArray
     * @param string $delimiter
     * @param string|null $patternExtend default "-_\s" - You can extend the search pattern what will be replaced to delimiter
     * @return array
     */
    public function uncamelizeKeys(array $inputArray, string $delimiter = "_", ?string $patternExtend = "-_\s"): array
    {
        $tmp = [];
        foreach ($inputArray as $key => $value) {
            $tmp[$this->unCamelize($key, $delimiter, $patternExtend)] = $value;
        }
        return $tmp;
    }

    /**
     * Index an array to array existing index
     *
     * @param object|array $dataList
     * @param string $indexTo main index
     * @param string $value gives the array specific item
     * @param boolean $strictScalar give back only non-scalar values (if true, only int, string, stc)
     * @return array
     */
    public function indexTo($dataList, string $indexTo, string $value = null, bool $strictScalar = false): array
    {
        $dataList = $this->getArray($dataList);
        $returnArray = [];
        foreach ($dataList as $data) {
            $tmp = $this->getArray($data)[$value] ?? $data;
            if (!empty($data[$indexTo]) or $data[$indexTo] === 0) {
                if ($strictScalar) {
                    if (is_scalar($tmp)) {
                        $returnArray[$data[$indexTo]] = $tmp;
                    }
                    continue;
                } else {
                    $returnArray[$data[$indexTo]] = $tmp;
                }
            }
        }
        return $returnArray;
    }

    /**
     * Index an array to array existing index
     *
     * @param object|array $dataList
     * @param string $indexTo main index
     * @return array
     */
    public function indexToMulti($dataList, string $indexTo): array
    {
        $dataList = $this->getArrayRecursive($dataList);
        $returnArray = [];
        foreach ($dataList as $data) {
            if (!empty($data[$indexTo]) or $data[$indexTo] === 0) {
                $returnArray[$data[$indexTo]][] = $data;
            }
        }
        return $returnArray;
    }

    /**
     * Get 1 column values from object list
     *
     * @param array|NativeArray|object $dataList
     * @param string $value
     * @return array
     */
    public function objectColumnValues($dataList, string $value): array
    {
        $dataList = $this->getArray($dataList);
        return array_column($dataList, $value);
    }


    /**
     * Create array of object using toArray if exists or typecast
     *
     * @param object $dataList
     * @return array
     */


    public function getArray($dataList): array
    {
        if (!is_array($dataList) and $dataList !== null) {
            if (method_exists($dataList, "toArray")) {
                return $dataList->toArray();
            }
            return (array) $dataList;
        }
        return $dataList ?? [];
    }

    /**
     * Get array from object list recursively
     *
     * @param array|NativeArray|object $dataList
     * @return array
     */
    public function getArrayRecursive($dataList): array
    {
        array_walk_recursive($dataList, [$this, "getArrayReference"]);
        return $dataList;
    }

     /**
     * Create array of object using toArray if exists or typecast
     *
     * @param array reference object $dataList
     * @return void
     */
    private function getArrayReference(&$dataList): void
    {
        if (!is_array($dataList)) {
            if (method_exists($dataList, "toArray")) {
                $dataList = $dataList->toArray();
            } elseif (!empty($dataList) and !is_scalar($dataList)) {
                $dataList = (array) $dataList;
            }
        }
    }

    /**
     * Format a number with grouped thousands
     * @see https://www.php.net/manual/en/function.number-format.php
     *
     * @param required int|decimal  $amount
     * @param int $decimals number of decimal numbers
     * @param array $delimiters define delimiters, first is decimal separator , second is thousands separator
     * @return string
     */

    public function formatAmount($amount, $decimals = 0, $delimiters = [".", ","]): string
    {
        return number_format($amount, $decimals, ...$delimiters);
    }

    /**
     * N dimension array flatter function
     *
     * @param any $array
     * @param array $exclude
     * @param boolean $holdOriginKeys
     * @return array
     */
    public function arrayFlatter($array, array $exclude = [], bool $holdOriginKeys = false): array
    {
        if (!is_array($array)) {
            $array = $this->getArray($array);
        }
        $result = array();
        foreach ($array as $key => $value) {
            if (in_array($key, $exclude, true)) {
                continue;
            }

            if (is_array($value)) {
                $result = array_merge($result, $this->arrayFlatter($value, $exclude, $holdOriginKeys));
            } else {
                if ($holdOriginKeys and !is_numeric($key)) {
                    $result[$key] = $value;
                } elseif (!in_array($value, $result)) {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }

    /**
     * Validate JSON string
     *
     * @param string|null $json
     * @return boolean
     */
    public function isJson(?string $json): bool
    {
        if (json_decode($json) !== null && !is_numeric(json_decode($json))) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Check an array|NativeArray|objectvalues empty, return false if one of these NOT empty
     *
     * @param array|NativeArray|object $source
     * @return boolean
     */

    public function fullEmpty($source = []): bool
    {
        return !$this->fullFilled($source);
    }

    /**
     * Return false if at least one element is empty
     *
     * @param array|NativeArray|object $source
     * @return boolean
     */
    public function fullFilled($source): bool
    {
        $source = $this->getArray($source);
        if (empty($source)) {
            return false;
        }
        foreach ($source as $value) {
            if (is_array($value)) {
                if (!$this->fullfilled($value)) {
                    return false;
                }
            } elseif (empty($value)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Return the empty element key
     *
     * @param array|NativeArray|object $source
     * @return array
     */
    public function getEmpties($source): array
    {
        return array_keys($source, false, false);
    }

    /**
     * Check associative array required values
     *
     * @param array $source array
     * @param array $keys keylist
     * @param boolean $checkOnlyKeys if true, check only key exists, false value should be filled
     * @return boolean
     */
    public function checkRequired(array $source, array $keys, bool $checkOnlyKeys = true): bool
    {
        $existingKeys = array_keys($source);
        foreach ($keys as $key) {
            if (!in_array($key, $existingKeys)) {
                return false;
            }
            if (!$checkOnlyKeys and empty($source[$key])) {
                return false;
            }
        }
        return true;
    }


    /**
     * Return constant value if exists or return defaultValue
     *
     * @param string $constName
     * @param any $defaultValue
     * @return mixed
     */
    public function constValue(string $constName, $defaultValue = false)
    {
        return defined($constName) ? constant($constName) : $defaultValue;
    }

    /**
     * Remove tailing 0 and . (dot) from the end of string
     *
     * @param string|null $subject
     * @return string|null
     */
    public function removeTail(?string $subject): ?string
    {
        if ($subject == 0 or !is_numeric($subject) or $this->isInteger($subject)) return $subject;
        return rtrim(rtrim($subject, 0), ".");
    }

    public function isInteger($input): bool
    {
        return (ctype_digit(strval($input)));
    }

    /**
     * Replace accented characters
     *
     * @param string $string
     * @return string
     */
   protected function clearSpecials(string $string): string
   {
       return iconv('utf8', 'ascii//TRANSLIT', $string);
   }

}
