<?php

declare(strict_types=1);

namespace Microframe\Helper;

use Microframe\DI;

trait RequiredArgsConstructor
{
    public function __construct()
    {
        (new DI())->autowire->wireClass($this);
        if (method_exists($this, 'autoloaded')) {
            $this->autoloaded(...func_get_args());
        }
    }
}