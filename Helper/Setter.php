<?php

declare(strict_types=1);

namespace Microframe\Helper;

use Microframe\DI;
use ReflectionProperty;

trait Setter
{
    public function __set(string $name, $value)
    {
        $type = (new ReflectionProperty($this, $name))->getType()->getName();
        if (!($value instanceof $type)) {
            settype($value, $type);
        }

        $fnc = "set".ucfirst($name);

        if (method_exists($this, $fnc)) {
            $this->$fnc($value);
        } else {
            $valueType = ($type == 'float' and gettype($value) == 'double') ? 'float' : gettype($value);
            if ((substr($valueType, 0, 3) !== substr($type, 0, 3)) and !($value instanceof $type)) {
                $value = (new DI())->conversionStorage->convert($value, $type);
            }
            $this->$name = $value;
        }
    }
}