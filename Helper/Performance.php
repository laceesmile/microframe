<?php

declare(strict_types=1);

namespace Microframe\Helper;

trait Performance
{
    private int $start;
    private int $stop;
    public function start()
    {
        $this->start = hrtime(true);
    }

    public function stop()
    {
        $this->stop = hrtime(true);
    }

    public function printTime()
    {
        echo static::class . ' run ' . ($this->stop - $this->start) / 1e+6 . ' miliseconds' . PHP_EOL;
    }
}