<?php

declare(strict_types=1);

namespace Microframe\Helper;

trait Arrayable 
{
    /**
     * Convert static to assoc array
     *
     * @return array
     */
    public function toArray(): array
    {
        return array_map(function ($item) {
            if (in_array(gettype($item), ['object', 'string']) and method_exists($item, 'toArray')) return $item->toArray();
            return $item;
        }, get_object_vars($this));
    }

    public function toFilledArray(): array
    {
        return array_filter($this->toArray());
    }
}