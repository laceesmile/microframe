<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

class Submit extends AbstractElement
{
    public function __construct(string $name, ?array $options = [])
    {
        $this->tag("input");
        if (!isset($options["value"])) {
            $options["value"] = $name;
        }
        $options["type"] = $options["type"] ?? "submit";
        parent::__construct($name, $options);
    }

    public function build(array $additional = [])
    {
        $options = array_merge($this->options, $additional);
        unset($options["name"]);
        return $this->wrap($this->buildFromArray($options));
    }

    public function validate()
    {
        return true;
    }
}