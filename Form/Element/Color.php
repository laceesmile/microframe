<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class Color extends Input
{
    const TYPE = "color";
}