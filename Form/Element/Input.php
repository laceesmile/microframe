<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

abstract class Input extends AbstractElement
{
    const TYPE = "text";
    
    public function __construct(string $name, ?array $options = [])
    {
        $this->tag("input");
        $options["type"] = static::TYPE;
        parent::__construct($name, $options);
    }

    public function build(array $additional = [])
    {
        if ($this->hasValue()) {
            $this->options["value"] = $this->getValue();
        }
        return $this->wrap($this->buildFromArray(array_merge($this->options, $additional)));
    }
}