<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class Checkbox extends Input
{
    const TYPE = "checkbox";

    public function build(array $additional = [])
    {
        if ($this->hasValue() and $this->getValue() != 0) {
            $this->options["value"] = $this->getValue();
            $this->options["checked"] = "checked";
        }
        return $this->wrap($this->buildFromArray(array_merge($this->options, $additional)));
    }
}