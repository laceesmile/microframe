<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class Radio extends Input
{
    const TYPE = "radio";
    
    public function __construct(string $name, ?array $options = [])
    {
        unset($this->options["name"]);
        parent::__construct($name, $options);
        $this->setLabel($this->getName());
    }
}