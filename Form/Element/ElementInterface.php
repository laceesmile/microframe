<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use JsonSerializable;
use Microframe\Html\TagInterface;

interface ElementInterface extends TagInterface, JsonSerializable
{
    public function render(): string;

    public function setLabel(string $title, ?array $options = []);

    public function getName(): string;

    public function setValue($value = null): self;

    public function getValue();

    public function validate();
}