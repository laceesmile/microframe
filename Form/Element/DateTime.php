<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class DateTime extends Input
{
    const TYPE = "datetime-local";
}