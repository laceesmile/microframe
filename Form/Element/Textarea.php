<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Common\NativeArray;

class Textarea extends AbstractElement
{
    public function __construct(string $name, ?array $options = [])
    {
        $this->tag("textarea");
        parent::__construct($name, $options);
    }

    public function build(array $additional = [])
    {
        if ($this->getValue() instanceof NativeArray) {
            $this->setValue($this->getValue()[0]);
        }
        return $this->wrap($this->buildFromArray(array_merge($this->options, $additional))) . $this->getValue() . $this->close();
    }
}