<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Illuminate\Database\Eloquent\Collection;
use InvalidArgumentException;
use Microframe\Common\NativeArray;
use Microframe\Helper\Helper;
use Microframe\Html\Element\Option;
use Microframe\Mvc\Model;

class Select extends AbstractElement
{
    use Helper;

    private $possibilities;
    private $default = null;

    public function __construct(string $name, $possibilities, ?array $options = [])
    {
        if ($possibilities instanceof Collection) {
            $possibilities = $this->formatCollectionResult($possibilities, $name);
        } elseif (!is_array($possibilities) and !($possibilities instanceof NativeArray)) {
            throw new InvalidArgumentException("Form only accept array, NativeArray or " . Collection::class . " object.", 500);
        }
        $this->possibilities = $possibilities;
        $this->tag("select");
        if (isset($options["multiple"]) and $options["multiple"] == "multiple") {
            // this append [] to multiple select if there no []
            $name = rtrim($name, "[]") . "[]";
        }
        parent::__construct($name, $options);
        $this->buildDefault();
    }

    public function buildDefault(array $additional = [])
    {
        foreach ($this->possibilities as $key => $possibility) {
            $option = $this->buildOption($key, $possibility, $additional);
            $this->possibilities[$key] = $option;
        }
    }

    public function selectValues()
    {
        $elementValue = $this->getValue() ?? $this->default;
        if ($elementValue == null) {
            return;
        }

        if (!($elementValue instanceof NativeArray) and !is_array($elementValue)) {
            foreach ($this->possibilities as $key => $possibility) {
                if ($key == $elementValue) {
                    $possibility->selected();
                    break;
                }
            }
        }
    }

    public function build(array $additional = [])
    {
        $options = array_merge($this->options, $additional);
        $empty = null;
        if (isset($options["allowEmpty"])) {
            $options["allowclear"] = "true";
            $options["placeholder"] = $options["allowEmpty"];
            unset($options["allowEmpty"]);
            $empty = (new Option(null, []))->toString();
        }

        $this->html = $this->wrap($this->buildFromArray($options)) . $empty;

        $this->selectValues();
        foreach ($this->possibilities as $possibility) {
            $this->html .= $possibility->toString();
        }
        return $this->html .= $this->close();
    }


    /**
     * @param defaultIndex mixed
     * set default value
     */
    public function setDefault($default): self
    {
        $this->default = $default;

        return $this;
    }

    private function buildOption($key, $possibility, array $additional = [])
    {
        if ($possibility instanceof NativeArray) {
            $possibility = $possibility->toArray();
        }
        if (is_array($possibility)) {
            $possibility["value"] = array_key_exists("value",$possibility) ? $possibility["value"] : $key;
            return new Option($possibility["content"] ?? $key, array_merge($possibility, $additional));
        } else {
            $options = [];
            $options["value"] = $key;
            return new Option($possibility, array_merge($options, $additional));
        }
    }

    private function formatCollectionResult(Collection $collection, string $name): array
    {
        if (empty($collection) or count($collection) == 0) {
            return [];
        }
        $model = $collection[0];
        $primaryKey = $this->getFirstKey($model);
        $secondKey =  $this->getSecondKey($model, $name,$primaryKey);
        return $this->indexTo($collection, $primaryKey, $secondKey);
    }

    private function getFirstKey(Model $model): string
    {
        $keys = array_keys($model->getOriginal());
        $defaultPrimaryKey = $model->getKeyName();
        return in_array($defaultPrimaryKey, $keys) ? $defaultPrimaryKey : $keys[0];
    }

    private function getSecondKey(Model $model, string $name, string $primaryKey = "id"): string
    {
        $keys = array_keys($model->getOriginal());
        unset($keys[array_search($primaryKey, $keys)]);
        $keys = array_values($keys);
        if (array_search($name, $keys)) {
            return $name;
        }
        return $keys[0];
    }
    
}