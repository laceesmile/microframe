<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class File extends Input
{
    const TYPE = "file";
    public function __construct(string $name, ?array $options = [])
    {
        unset($this->options["value"]);
        parent::__construct($name, $options);
    }
}