<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

class Button extends AbstractElement
{
    public function __construct(string $name, ?array $options = [])
    {
        $this->tag("button");
        if (!isset($options["value"])) {
            $options["value"] = $name;
        }
        parent::__construct($name, $options);
    }

    public function build(array $additional = [])
    {
        $options = array_merge($this->options, $additional);
        $value = $options["value"];
        unset($options["name"], $options["value"]);
        return $this->wrap($this->buildFromArray($options)) . $value . $this->close();
    }

    public function validate()
    {
        return true;
    }
}