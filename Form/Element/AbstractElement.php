<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\ElementInterface;
use Microframe\Html\AbstractTag;
use Microframe\Html\Element\Label;
use Microframe\Validator\ValidatorInterface;

abstract class AbstractElement extends AbstractTag implements ElementInterface
{
    private bool $isCleared = false;

    protected string $name;
    protected array $options = [];
    protected Label $label;
    protected $value = null;
    protected array $validators = [];

    public bool $isValid = true;

    /**
     * Initialize an element
     *
     * @param string $name - The name of the element.
     * @param array|null $options - Additional attributes for the html element.
     */
    public function __construct(string $name, ?array $options = [])
    {
        $this->name = $name;
        if (!isset($options["id"])) {
            $options["id"] = $this->getName() ?? $options["name"];
        }
        if (!isset($options["name"])) {
            $options["name"] = $this->getName();
        }
        $this->options = $options;
    }

    /**
     * Set the label for an element
     *
     * @param string $title
     * @param array|null $options
     * @return self
     */
    public function setLabel(string $title, ?array $options = []): self
    {
        $options["for"] = $this->options["id"] ?? $this->name;
        $this->label = new Label($title, $options);
        
        return $this;
    }

    public function clear(): self
    {
        $this->setValue(null);

        $this->isCleared = true;

        return $this;
    }

    /**
     * Add a validator to the element
     *
     * @param ValidatorInterface $validator
     * @return self
     */
    public function addValidator(ValidatorInterface $validator): self
    {
        $this->validators[] = $validator;

        return $this;
    }

    /**
     * Add options to the element as array, merged by already added attributes (priority on this)
     *
     * @param array $options - Associative array by options => value
     * @return self
     */
    public function setOptions(array $options): self
    {
        $this->options = array_merge($this->options, $options);

        return $this;
    }

    /**
     * Add a html attribute to the element.
     *
     * @param string $name - Attribute name
     * @param string $value - Attribute value
     * @return self
     */
    public function addOption(string $name, string $value): self
    {
        $this->options[$name] = $value;

        return $this;
    }

    public function getOption(string $name): ?string
    {
        return $this->options[$name] ?? null;
    }

    public function validate()
    {
        $valid = true;
        foreach ($this->validators as $validator) {
            if (!$validator->validate($this)) {
                $this->isValid = false;
                return false;
            }
        }
        return $valid;
    }

    public function label(array $additional = []): ?AbstractTag
    {
        if ($this->label) {
            return $this->label->addOptions($additional);
        }
        return null;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function hasValue(): bool
    {
        return isset($this->value) and $this->value !== null;
    }

    public function setValue($value = null): ElementInterface
    {
        if ($this->isCleared === true) return $this;

        $this->value = $value;

        return $this;
    }

    public function __serialize(): array
    {
        return [$this->name, $this->value, $this->options, $this->validators, $this->isValid];
    }

    public function __unserialize($serialized): void
    {
        list($this->name, $this->value, $this->options, $this->validators, $this->isValid) = unserialize($serialized);
    }

    public function jsonSerialize(): mixed
    {
        return ["name" => $this->name, "value" => $this->getValue(), "label" => $this->label(), "options" => $this->options, "isValid" => $this->isValid];
    }
}