<?php

declare(strict_types=1);

namespace Microframe\Form\Element;

use Microframe\Form\Element\Input;

class Disabled extends Input
{
    const TYPE = "text";

    public function __construct(string $name, ?array $options = [])
    {
        $this->tag("input");
        $options["type"] = static::TYPE;
        $options["disabled"] = "disabled";
        parent::__construct($name, $options);
    }

    public function build(array $additional = [])
    {
        if ($this->hasValue()) {
            $this->options["value"] = $this->getValue();
        }
        return $this->wrap($this->buildFromArray(array_merge($this->options, $additional)));
    }
}