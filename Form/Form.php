<?php

declare(strict_types=1);

namespace Microframe\Form;

use InvalidArgumentException;
use Microframe\Common\NativeArray;
use Microframe\File\File;
use Microframe\Form\Element\AbstractElement;
use Microframe\Form\Element\Disabled;
use Microframe\Form\Element\File as FileElement;
use Microframe\Form\FormInterface;
use Microframe\Helper\Helper;
use Microframe\Html\AbstractTag;
use Microframe\Mvc\Model;
use Microframe\Security\CsrfProtection;

/**
 * Form object
 * Extends this by all of your form
 * @abstract
 */
abstract class Form extends AbstractTag implements FormInterface
{
    private const DEFAULT_METHOD = "POST";
    
    use Helper;
    use CsrfProtection;

    private ?string $method = null;
    private array $elements = [];
    private array $options = [];
    private ?string $action = null;
    private NativeArray $elementValues;
    private bool $isValid = true;

    protected $original;

    /**
     * Initialize a form
     * Cannot overwrite
     *
     * @param Model|NativeArray|array $values
     * @param array $options
     * @final
     */
    public final function __construct($values = null, array $options = [])
    {
        $this->tag("form");

        $this->original = $this->getArray($values) ?? [];

        $this->setElementValues($values);

        $this->setOptions($options);
        $this->initialize($this->elementValues, $options);
        $this->fillValues();
        $this->secureCsrf();
    }

    /**
     * Initialize a form
     * Abstract, use this instead of __construct
     *
     * @param NativeArray|null $values
     * @param array $options
     * @return void
     */
    abstract public function initialize(?NativeArray $values = NULL, array $options = []);

    protected function fillValues()
    {
        foreach ($this->getElements() as $elementName => $element) {
            if ($element instanceof Disabled) continue;
            $tmp = substr_compare($elementName, "[]", -2) === 0 ? substr_replace($elementName, "", -2) : $elementName;
            if (!$this->constValue("MICROFRAME_DISABLE_REQUEST_PARAMS_NAME_CAMELIZE", false)) {
                $tmp = $this->camelize($tmp);
            }
            $elementName = $tmp;
            $value = $this->elementValues[$elementName] ?? null;
            $element->setValue($value);
        }
    }

    public function isValid(array $values = []): bool
    {
        if (!empty($values)) {
            $this->setElementValues($values);
            $this->fillValues();
        }
        foreach ($this->getElements() as $elementName => $element) {
            $validElement = $element->validate();
            if ($validElement !== true) {
                $this->isValid = false;
            }
        }
        return $this->isValid;
    }

    protected function getElementValue(string $elementName)
    {
        $elementName = preg_replace('/\[\d+\]$/', "", $elementName);
        if (!$this->constValue("MICROFRAME_DISABLE_REQUEST_PARAMS_NAME_CAMELIZE", false)) {
            $elementName = $this->camelize($elementName);
        }
        if ($this->hasValue($elementName)) {
            return $this->elementValues[$elementName];
        }
        return null;
    }

    public function add(AbstractElement $element): FormInterface
    {
        if ($element instanceof FileElement) {
            $this->addOption("enctype", "multipart/form-data");
        }
        $element->setValue($this->getElementValue($element->getName()));

        $this->elements[$element->getName()] = $element;

        return $this;
    }

    public function isChanged(string $elementName): bool
    {
        if (!empty($this->original[$elementName])) {
            return $this->original[$elementName] != $this->getElementValue($elementName);
        }
        return empty($this->original[$elementName]) and $this->hasValue($elementName); 
    }

    public function getElement(string $elementName): ?AbstractElement
    {
        return $this->elements[$elementName] ?? null;
    }

    public function getElements(): array
    {
        return $this->elements;
    }

    public function addOption(string $optionName, string $value): void
    {
        $this->options[$optionName] = $value;
    }

    public function build(array $additional = []): string
    {
        $params = [];
        if ($this->getMethod() !== null) {
            $params["method"] = $this->getMethod();
        }
        if ($this->getAction() !== null) {
            $params["action"] = $this->getAction();
        }
        $params = array_merge($params, $this->getOptions(), $additional);

        return $this->wrap($this->buildFromArray($params));
    }

    public function getMessages(bool $buildAsHtml = true): array
    {
        $response = [];
        $fnc = "getMessage";
        if (!$buildAsHtml) {
            $fnc = "getMessageString";
        }
        foreach ($this->getElements() as $element) {
            if (($msg = $element->$fnc()) !== null)
            $response[$element->getName()] = $msg;
        }
        return $response;
    }

    public function clearAllValues():self
    {
        foreach ($this->getElements() as $elementName => $element) {
            $element->setValue(null);
        }
        $this->elementValues = new NativeArray();
        return $this;
    }

    
    public function clearValues(array $elementNameList = []):self
    {
        $values = $this->getElementValues();
        foreach ($elementNameList as $elementName) {
            $values[$elementName] = null;
            $this->getElement($elementName)->clear();
        }
        $this->elementValues = new NativeArray($values);
        return $this;
    }

    public function clearValue(string $elementName): self
    {
        $this->getElement($elementName)->clear();

        $this->elementValues[$elementName] = null;

        return $this;
    }

    /**
     * Set the value of action
     *
     * @return  self
     */
    public function setAction(string $routeName, array $parameters = []): FormInterface
    {
        $link = $this->router->getRouteByName($routeName)->getUrl()->get($parameters);
        $this->action = $link;

        return $this;
    }

    public function has(string $name): bool
    {
        return isset($this->elements[$name]);
    }

    private function setElementValues($values = null): self
    {
        if ($values === null) {
            $values = $this->request->get();
        } elseif ($values instanceof Model) {
            $values = $values->toArray();
        } elseif (!is_array($values) and !($values instanceof NativeArray)) {
            throw new InvalidArgumentException("Form only accept array, NativeArray or " . Model::class . " object. " . ucfirst(gettype($values)) . " given.", 500);
        }
        if ($this->request->hasFile()) {
            $values = array_merge($values, $this->request->getFiles()->toArray());
        }
        $values = array_merge($this->request->get(), $values);
        $elementValues = [];
        foreach ($values as $key => $value) {
            if (is_scalar($value)) {
                $elementValues[$key] = htmlspecialchars((string) $value, ENT_HTML401);
            } elseif (is_array($value) or $value instanceof File or $value instanceof NativeArray) {
                $elementValues[$key] = $value;
            }
        }
        $this->elementValues = new NativeArray($elementValues);

        return $this;
    }

    public function hasValue(string $name): bool
    {
        return isset($this->elementValues[$name]);
    }

    /**
     * Get the value of action
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * Get the value of options
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    public function getOption(string $name)
    {
        return $this->options[$name] ?? null;
    }

    /**
     * Set the value of options
     *
     * @return  self
     */
    public function setOptions(array $options): self
    {
        if (isset($options["action"])) {
            $this->setAction($options["action"]);
            unset($options["action"]);
        }
        if (isset($options["method"])) {
            $this->setMethod($options["method"]);
            unset($options["method"]);
        }
        $this->options = $options;

        return $this;
    }

    /**
     * Get the value of name
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of method
     */
    public function getMethod(): ?string
    {
        return $this->method ?? self::DEFAULT_METHOD;
    }

    public function getIsValid(): bool
    {
        return $this->isValid;
    }

    /**
     * Set the value of method
     *
     * @return  self
     */
    public function setMethod(string $method): self
    {
        $this->method = strtoupper($method);

        return $this;
    }

    public function serialize(): string
    {
        return serialize([$this->elements, $this->elementValues, $this->tag, $this->method]);
    }

    public function unserialize($serialized): void
    {
        list($this->elements, $this->elementValues, $this->tag, $this->method) = unserialize($serialized);
    }

    public function jsonSerialize(): mixed
    {
        return ["elements" => $this->elements, "method" => $this->getMethod()];
    }

    /**
     * Get the value of elementValues
     */ 
    public function getElementValues()
    {
        return $this->elementValues;
    }
}
