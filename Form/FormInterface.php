<?php

declare(strict_types=1);

namespace Microframe\Form;

use JsonSerializable;
use Microframe\Common\NativeArray;
use Microframe\Form\Element\AbstractElement;
use Microframe\Html\TagInterface;

interface FormInterface extends TagInterface, JsonSerializable
{
    /**
     * Initialize form
     *
     * @param NativeArray|null $values
     * @param array $options
     * @return void
     */
    public function initialize(?NativeArray $values = null, array $options = []);

    /**
     * Close form tag
     *
     * @return string
     */
    public function close(): string;

    public function add(AbstractElement $element): self;

    public function getElement(string $name): ?AbstractElement;

    public function getElements(): array;

    public function setAction(string $routeName, array $parameters = []): self;

    public function isValid(): bool;
}