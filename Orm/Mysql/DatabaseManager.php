<?php

declare(strict_types=1);

namespace Microframe\Orm\Mysql;

use Microframe\Database\AbstractDatabaseManager;
use Microframe\Database\DatabaseManagerInterface;
use Illuminate\Database\Capsule\Manager as Capsule;
use Microframe\Database\Exception\ConnectionException;
use Microframe\Database\Exception\MissingExtensionException;

class DatabaseManager extends AbstractDatabaseManager implements DatabaseManagerInterface
{
    const SUPPORTED_DRIVERS = [
        'mysql',
        'pgsql',
        'sqlite',
        'sqlsrv'
    ];

    public function getDriverDependencies(): array
    {
        return [
            Capsule::class => ConnectionException::class,
        ];
    }

    public function getExtensionDependencies(): array
    {
        return [
            "pdo_mysql" => MissingExtensionException::class,
            "json" => MissingExtensionException::class,
            "mbstring" => MissingExtensionException::class,
        ];
    }

    public static function getSupportedDrivers(): array
    {
        return self::SUPPORTED_DRIVERS;
    }
}