<?php

declare(strict_types=1);

namespace Microframe\Orm\Mysql;

use Microframe\Database\ManagerInterface;
use Microframe\Orm\Mysql\DatabaseManager;
use Illuminate\Database\Capsule\Manager as Capsule;

class Manager extends DatabaseManager implements ManagerInterface
{
    private Capsule $capsule;

    public function __call(string $method, array $parameters = [])
    {
        return $this->capsule->$method(...$parameters);
    }

    public function buildConnection(array $connectionParams, bool $multiple = false)
    {
        $this->checkDependencies();

        $this->capsule = new Capsule();

        if ($multiple) {
            foreach ($connectionParams as $name => $connection){
                $this->capsule->addConnection($connection, $name);
            }
        } else {
            $this->capsule->addConnection($connectionParams);
        }
        
        $this->capsule->setAsGlobal();

        $this->capsule->bootEloquent();
    }

    public function getConnection($name = null)
    {
        return $this->capsule->getDatabaseManager()->connection($name);
    }

    public function close()
    {
        $this->capsule->getDatabaseManager()->disconnect();
    }
}