<?php

declare(strict_types=1);

namespace Microframe\Orm;

use Microframe\Orm\Mysql\Manager;
use Microframe\Database\ManagerInterface;
use Microframe\Database\AbstractConnection;
use Microframe\Database\Exception\ConnectionException;

class Connection extends AbstractConnection
{
    const ORM_CONNECTION_NAME = "sql";

    public string $name;
    
    static array $managers = [
        "mysql" => Manager::class,
    ];

    public function getConnection()
    {
        return $this->connection;
    }

    public function getName(): string
    {
        return $this->name ?? self::ORM_CONNECTION_NAME;
    }

    public function getManager(string $driverName): ManagerInterface
    {
        $driver = $this->getDriver($driverName);
        if (!is_null($driver)) {
            return new $driver();
        }
        throw new ConnectionException($this->driver);
    }

    private function getDriver(string $driverName): ?string
    {
        foreach (static::$managers as $manager) {
            $supportedDrivers = $manager::getSupportedDrivers();
            if (in_array($driverName, $supportedDrivers)) {
                return $manager;
            }
        }
        return null;
    }

    /**
     * Set the value of name
     *
     * @return  self
     */ 
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }
}