<?php

declare(strict_types=1);

namespace Microframe\Provider;

use Microframe\Common\RegisterTrait;
use Microframe\DI;
use Microframe\Interfaces\ProviderInterface;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Provider\Exception\ProviderException;
use Microframe\Storage\RuntimeMemory;

final class Provider implements RegistableInterface
{
    use RegisterTrait;

    public const DI_KEY = 'provided';

    public function has(string $name): bool
    {
        return RuntimeMemory::has($name);
    }

    public function get($name): mixed
    {
        return RuntimeMemory::get($name);
    }

    public function register(ProviderInterface $provider)
    {
        $di = new DI();
        $diContainer = RuntimeMemory::get(self::DI_KEY);
        foreach ($provider->provide() as $name => $object) {
           $di->set($this->getName($name), function() use($object) {
                return is_string($object) ? new $object() : $object;
            }, true, is_string($object) ? $object : get_class($object)); 
            $diContainer->getValue()->$name = $object;
        }
    }

    private function isRegistable(string $name): bool
    {
        return is_subclass_of($name, RegistableInterface::class);
    }

    private function getName(string $name): string
    {
        if ($this->isRegistable($name)) {
            return $name::getRegisterName();
        }
        throw new ProviderException($name);
    }
}