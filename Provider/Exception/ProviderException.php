<?php

declare(strict_types=1);

namespace Microframe\Provider\Exception;

use Microframe\Common\Exception\MicroframeException;
use Microframe\Interfaces\RegistableInterface;

class ProviderException extends MicroframeException
{
    public function __construct(string $objectName, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Object " . $objectName . " should implement " . RegistableInterface::class;
    }
}