<?php

namespace Microframe\Cache;

interface CacheObjectInterface
{
    /**
     * Cache contained object constructor.
     *
     * @param $value value to hold in cache
     * @param integer $lifeTime in seconds
     */
    public function __construct($value, int $lifeTime = 0);

    /**
     * Get the value of cache object
     *
     * @return mixed
     */
    public function getValue();

    /**
     * renew a session object time
     *
     * @param integer $lifeTime
     * @return boolean
     */
    public function reNew(int $lifeTime = 0): bool;

    
}