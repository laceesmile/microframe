<?php

declare(strict_types=1);

namespace Microframe\Cache\Redis;

use Microframe\Cache\CacheInterface;
use Microframe\Common\NullObject;
use Microframe\Common\ClassBuilder;
use \Redis as RedisCluster;

final class Redis implements CacheInterface
{
    private RedisCluster|NullObject $redis;
    private ClassBuilder $builder;
    
    public function __construct(ClassBuilder $builder = new ClassBuilder(['host' => 'localhost']))
    {
        $this->builder = $builder;
        $this->start();
    }

    public function test(?string $testMessage = 'Hello Redis!'): bool
    {
        return $this->redis->ping($testMessage) == $testMessage;
    }

    public function get(string $name): mixed
    {
        return $this->redis->get($name) ?? null;
    }

    public function set(string $name, $value, int $lifeTime = 0): void
    {
        $this->redis->set($name, $value, $lifeTime);
    }

    public function delete(string $name): bool
    {
        return $this->redis->del($name);
    }

    public function has(string $name): bool
    {
        return !!$this->redis->exists($name);
    }

    public function destroy(): bool
    {
        $this->clear();
        return $this->redis->close();
    }

    public function clear(): bool
    {
        return $this->redis->flushAll();
    }

    public function pause(bool $isPaused = true): bool
    {
        if ($isPaused) {
            $this->redis->close();
            $this->redis = new NullObject();
        } else {
            $this->start();
        }
        return true;
    }

    public function start(): bool
    {
        $this->redis = new RedisCluster();
        $this->redis->pconnect(
            ...$this->builder->getAll()
        );
        return true;
    }
}
