<?php

namespace Microframe\Cache;

interface CacheInterface
{
    /**
     * Get a cache variable by name
     *
     * @param string $name
     * @return mixed
     */
    public function get(string $name): mixed;

     /**
      * Set a cache variable
      *
      * @param string $name
      * @param mixed $value
      * @param integer $lifeTime Cache variable lifetime in seconds - default 0 => not expired
      * @return void
      */
    public function set(string $name, $value, int $lifeTime = 0): void;

    /**
     * Delete a cache variable
     *
     * @param string $name
     * @return boolean
     */
    public function delete(string $name): bool;

    /**
     * Check variable if exists in cache
     *
     * @param string $name
     * @return boolean
     */
    public function has(string $name): bool;

    /**
     * Destroy the cache
     *
     * @return boolean
     */
    public function destroy(): bool;

    /**
     * Clear all cache variable
     * 
     * @return bool
     */
    public function clear(): bool;

    /**
     * Pause the cache write
     * 
     * @return bool
     */
    public function pause(): bool;

    /**
     * Start the caching
     * 
     * @return bool
     */
    public function start(): bool;
}
