<?php

declare(strict_types=1);

namespace Microframe\Cache\Session;

use Microframe\Cache\CacheObjectInterface;

class SessionObject implements CacheObjectInterface
{
    private int $lifeTime = 0;
    private $value = null;
    private int $validTill = 0; 

    public function __construct($value, int $lifeTime = 0)
    {
        $this->value = $value;
        $this->lifeTime = $lifeTime;
        $this->validTill = time() + $lifeTime;
    }

    public function getValue()
    {
        if ($this->lifeTime == 0) {
            return $this->value;
        } elseif ($this->validTill <= time()) {
            $this->value = null;
        }
        return $this->value;
    }

    public function reNew(int $lifeTime = 0): bool
    {
        if ($this->getValue() !== null) {
            $this->lifeTime = $lifeTime;
            $this->validTill = time() + $lifeTime;
            return true;
        }
        return false;
    }
}