<?php

declare(strict_types=1);

namespace Microframe\Cache\Session;

use Microframe\Cache\CacheInterface;
use Microframe\Cache\Session\Exception\SessionExpireException;
use Microframe\Cache\Session\Exception\SessionHandlerException;
use Microframe\Cache\Session\SessionObject;
use SessionHandlerInterface;
use Throwable;

final class Session implements CacheInterface
{
    public function __construct(bool $start = true)
    {
        if (session_status() == PHP_SESSION_NONE and $start) {
            $this->start();
        }
    }

    public function get(string $name, bool $destroy = false): mixed
    {
        if (!$this->exist($name)) {
            return null;
        }

        $object = $_SESSION[$name];
        if ($destroy === true) {
            $this->delete($name);
        }
        return $object->getValue();
    }

    public function set(string $name, $value, int $lifeTime = 0): void
    {
        $object = new SessionObject($value, $lifeTime);
        $_SESSION[$name] = $object;
    }

    /**
     * Renew a session variable lifetime
     *
     * @param string $name
     * @param integer $lifeTime
     * @return boolean
     */
    public function renew(string $name, int $lifeTime): bool
    {
        if (!$this->exist($name)) {
            return false;
        }

        $object = $_SESSION[$name];
        $object->reNew($lifeTime);
        $this->set($name, $object->getValue());
        return true;
    }

    public function delete(string $name): bool
    {
        unset($_SESSION[$name]);

        return true;
    }

    /**
     * Alias of delete
     *
     * @param string $name
     * @return boolean
     */
    public function remove(string $name): bool
    {
        return $this->delete($name);
    }

    public function has(string $name): bool
    {
        $object = $this->get($name);
        if ($object !== null) {
            return true;
        }
        else {
            return !$this->delete($name);
        }
    }

    public function destroy(): bool
    {
        $this->clear();
        
        return session_destroy();
    }

    public function clear(): bool
    {
        $_SESSION = [];

        return session_unset();
    }

    public function pause(): bool
    {
        return session_write_close();
    }

    public function start(): bool
    {
        return session_start();
    }

    /**
     * Check the variable exists in session
     *
     * @param string $name
     * @return boolean
     */
    private function exist(string $name): bool
    {
        return isset($_SESSION[$name]);
    }

    public function setGcLifetime(int $sec): self
    {
        ini_set("session.gc_maxlifetime", (string) $sec);

        return $this;
    }

    public function getGcLifetime(): int
    {
        return (int) ini_get("session.gc_maxlifetime");
    }

    /**
     * Set cache expire time globally
     * @see session_cache_expire()
     *
     * @param integer $minutes
     * @return self
     * 
     * @throws SessionExpireException;
     */
    public function setExpire(int $minutes): self
    {
        if ($this->getDelimiter() === "nocache") {
            throw new SessionExpireException();
        }
        session_cache_expire($minutes);

        return $this;
    }

    public function getExpire(): int
    {
        return session_cache_expire();
    }

    /**
     * Set the session name
     *
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        session_name($name);

        return $this;
    }

    /**
     * Get the session name
     *
     * @return string
     */
    public function getName(): string
    {
        return session_name();
    }

    /**
     * Set the save handler
     * @see session_set_save_handler()
     *
     * @param SessionHandlerInterface $handler
     * @return self
     * 
     * @throws SessionHandlerException
     */
    public function setHandler(SessionHandlerInterface $handler): self
    {
        try {
            session_set_save_handler($handler);
        } catch (Throwable $e) {
            throw new SessionHandlerException($e->getMessage());
        }

        return $this;
    }

    /**
     * Set the cache delimiter
     * use for setExpire()
     *
     * @param string $delimiter
     * @return self
     */
    public function setDelimiter(string $delimiter): self
    {
        session_cache_limiter($delimiter);

        return $this;
    }

    /**
     * Get the delimiter
     *
     * @return string
     */
    public function getDelimiter(): string
    {
        return session_cache_limiter();
    }


    public function getHandler()
    {
        return ini_get("session.save_handler");
    }

    public function setPath(string $path = ""): self
    {
        session_save_path($path);

        return $this;
    }

    public function getPath(): string
    {
        return session_save_path();
    }

    public function setSessionId(string $id): self
    {
        session_id($id);

        return $this;
    }

    public function getSessionId(): string
    {
        return session_id();
    }

    public function regenerateId(bool $deleteOld = false): self
    {
        session_regenerate_id($deleteOld);
        
        return $this;
    }
}
