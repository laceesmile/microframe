<?php

declare(strict_types=1);

namespace Microframe\Cache\Session\Exception;

use Microframe\Common\Exception\MicroframeException;

class SessionHandlerException extends MicroframeException
{
    public function __construct(string $message, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Unable to initialize handler. Message: $message. Read more: https://www.php.net/manual/en/function.session-set-save-handler.php";
    }
}