<?php

declare(strict_types=1);

namespace Microframe\Cache\Session\Exception;

use Microframe\Common\Exception\MicroframeException;

class SessionExpireException extends MicroframeException
{
    /**
     * Constructor
     * 
     * Initialize an error about session expire.
     */
    public function __construct(int $code = 500)
    {
        $this->code = $code;
        $this->message = "Before setExpire you should call setDelimiter with different like 'nocache'. Read more: https://www.php.net/manual/en/function.session-cache-expire.php";
    }
}