<?php

declare(strict_types=1);

namespace Microframe\Database;

use Microframe\Database\ManagerInterface;

interface ConnectionInterface
{
    public function __construct(array $connectionParams);

    /**
     * Get the connection object
     *
     * @return void
     */
    public function getConnection();

    public function getManager(string $driverName): ManagerInterface;

    public function getName(): string;
}