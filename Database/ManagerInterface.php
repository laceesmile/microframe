<?php

declare(strict_types=1);

namespace Microframe\Database;

interface ManagerInterface
{
    /**
     * Build a connection
     *
     * @param array $connectionParams
     * @return void
     */
    public function buildConnection(array $connectionParams);

    /**
     * Get the connection
     *
     * @param string|null $name
     * @return void
     */
    public function getConnection(string $name = null);

    /**
     * Close the connection
     *
     * @return void
     */
    public function close();
}