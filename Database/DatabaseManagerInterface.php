<?php

declare(strict_types=1);

namespace Microframe\Database;

interface DatabaseManagerInterface
{
    /**
     * Get the manager supported driverlist
     *
     * @return array
     */
    public static function getSupportedDrivers(): array;

    /**
     * Get the driver dependencies
     *
     * @return array
     */
    public function getDriverDependencies(): array;

    /**
     * Get the driver PHP extension dependencies
     *
     * @return array
     */
    public function getExtensionDependencies(): array;
} 