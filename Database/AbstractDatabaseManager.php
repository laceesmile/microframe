<?php

declare(strict_types=1);

namespace Microframe\Database;

abstract class AbstractDatabaseManager implements DatabaseManagerInterface
{
    protected function checkDependencies(): void
    {
        $this->checkExtensionDependencies();

        $this->checkDriverDependencies();
    }

    protected function checkDriverDependencies(): bool
    {
        $requiredDriverList = $this->getDriverDependencies();
        foreach ($requiredDriverList as $driver => $errorClass) {
            if (!class_exists($driver)) {
                throw new $errorClass($driver);
            }
        }
        return true;
    }

    protected function checkExtensionDependencies(): bool
    {
        
        $requiredExtensionList = $this->getExtensionDependencies();
        foreach ($requiredExtensionList as $extension => $errorClass) {
            if (!extension_loaded($extension)) {
                throw new $errorClass($extension);
            }
        }
        return true;
    }

    public function getDriverDependencies(): array
    {
        return [];
    }

    public function getExtensionDependencies(): array
    {
        return [];
    }
}