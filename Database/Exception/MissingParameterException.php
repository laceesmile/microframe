<?php

declare(strict_types=1);

namespace Microframe\Database\Exception;

use Microframe\Common\Exception\MicroframeException;

class MissingParameterException extends MicroframeException
{
    public function __construct(string $parameterName, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Missing " . $parameterName . " parameter from connection parameters!";
    }
}