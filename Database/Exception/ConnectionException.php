<?php

declare(strict_types=1);

namespace Microframe\Database\Exception;

use Microframe\Common\Exception\MicroframeException;

class ConnectionException extends MicroframeException
{
    public function __construct(string $driverName, int $code = 404)
    {
        $this->code = $code;
        $this->message = "Driver {$driverName} not found!";
    }
}