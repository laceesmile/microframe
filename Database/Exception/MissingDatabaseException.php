<?php

declare(strict_types=1);

namespace Microframe\Database\Exception;

use Microframe\Database\Exception\MissingParameterException;

class MissingDatabaseException extends MissingParameterException
{
    public function __construct()
    {
        $this->parameterName = "database";
        parent::__construct();
    }
}