<?php

declare(strict_types=1);

namespace Microframe\Database\Exception;

use Microframe\Database\Exception\MissingParameterException;

class MissingDriverException extends MissingParameterException
{
    public function __construct()
    {
        $this->parameterName = "driver";
        parent::__construct();
    }
}