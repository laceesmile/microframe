<?php

declare(strict_types=1);

namespace Microframe\Database\Exception;

use Microframe\Common\Exception\MicroframeException;

class MissingExtensionException extends MicroframeException
{
    public function __construct(string $extensionName, int $code = 404)
    {
        $this->code = $code;
        $this->message = "PHP extension {$extensionName} not loaded!";
    }
}