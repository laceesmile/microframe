<?php

declare(strict_types=1);

namespace Microframe\Database;

use Microframe\Database\Exception\MissingParameterException;

abstract class AbstractConnection implements ConnectionInterface
{
    /**
     * Connection manager
     * @required
     * 
     * @var ManagerInterface
     */
    protected ManagerInterface $manager;

    
    protected $connection = null;

    protected string $driver = "mysql";
    protected string $database;
    protected string $host;
    protected string $username;
    protected string $password;
    protected string $port;

    protected string $name;


    private array $defaultRequiredParams = [
        "driver",
        "database",
        "host",
        "username",
        "password",
    ];

    /**
     * Initialize new database connection
     *
     * @param array $connectionParams
     * @param boolean $multiple Set to true if more than one connection initialized
     */
    public function __construct(array $connectionParams, bool $multiple = false)
    {
        $this->getDriverName($connectionParams);

        if (!$multiple){

            $this->checkParams($connectionParams);

            $this->manager = $this->getManager($this->driver);

            $this->manager->buildConnection($connectionParams);
            
            $this->connection = $this->manager->getConnection();
        } else {

            $this->manager = $this->getManager($this->driver);

            $this->manager->buildConnection($connectionParams, $multiple);
        }
    }

    /**
     * Get the connection name
     *
     * @return string
     */
    abstract function getName(): string;

    /**
     * Get the driver name from params or default (mysql)
     *
     * @param array $connectionParams
     * @return string
     */
    protected function getDriverName(array $connectionParams): string
    {
        if (isset($connectionParams["driver"])) {
            $this->driver = $connectionParams["driver"];
        }
        return $this->driver;
    }

    /**
     * Check all required parameter available in config
     * ["driver", "database", "host", "username", "password"]
     * 
     * @param array $connectionParams
     * @param array $requiredColumns
     * @return boolean
     * 
     * @throws MissingParameterException
     */
    protected function checkParams(array $connectionParams, array $requiredColumns = []): bool
    {
        $requiredColumns = !empty($requiredColumns) ? $requiredColumns : $this->defaultRequiredParams;       

        foreach ($requiredColumns as $columnName) {
            if (!isset($connectionParams[$columnName])) {
                throw new MissingParameterException($columnName);
            } else {
                $this->$columnName = $connectionParams[$columnName];
            }
        }
        return true;
    }

}