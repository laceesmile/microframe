<?php

declare(strict_types=1);

namespace Microframe\File;

use League\Csv\Writer;
use Microframe\Common\NativeArray;
use Microframe\File\AbstractFile;
use Microframe\Helper\Helper;

class Csv extends AbstractFile
{
    use Helper;
    
    protected $writer;
    protected $stream = null;

    public function __construct(string $file = null, bool $create = false)
    {
        $this->extension = "csv";
        $fileParams = pathinfo($file);
        $this->name = $fileParams["filename"];
        $this->file = $file;
        $this->place = $fileParams["dirname"];
        $this->fullName = $fileParams["basename"];
        $this->createPath($fileParams["dirname"]); // maybe this could be in the 'if'
        if ($create) {
            $this->file = $file . "." . $this->extension;
            $this->fullName = $fileParams["basename"] . "." . $this->extension;
            $this->stream = fopen($this->file, "w+");
        }
    }
    
    public function getContent(string $delimiter = ","): NativeArray
    {
        if ($this->content === null) {
            $this->read($delimiter);
        }
        return $this->content;
    }
    

    public function read(string $delimiter = ","): self
    {
        ini_set("auto_detect_line_endings", "1");
        $row = 0;
        $this->stream = fopen($this->file, "r");
        $content = [];
        if ($this->stream !== FALSE) {
            while (($data = fgetcsv($this->stream, 0, $delimiter)) !== FALSE) {
                if ($row === 0) {
                    $content["header"] = array_map([$this, "camelize"], $data);
                } else {
                    $content["content"][] = array_combine($content["header"], $data);
                }
                $row++;
            }
        }
        $this->content = new NativeArray($content);
        return $this;
    }

    public function write(array $header, array $content): self
    {
        $csv = $this->getWriter();
        $csv->insertOne($header);
        $csv->insertAll($content);
        return $this;
    }

    public function getWriter(): Writer
    {
        if ($this->writer === null) {
            $this->writer = Writer::createFromPath($this->file);
        }
        return $this->writer;
    }
    
    public function __destruct()
    {
        if ($this->stream !== null) {
            fclose($this->stream);
        }
    }

    /**
     * Get the value of stream
     */ 
    public function getStream()
    {
        return $this->stream;
    }
}