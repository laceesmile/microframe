<?php

declare(strict_types=1);

namespace Microframe\File;

use Microframe\Common\NativeArray;

class Json extends AbstractFile
{
    public function __construct(string $file)
    {
        parent::__construct($file.".json");
    }

    public function read(): self
    {
        if ($this->content === null) {
            $this->content = new NativeArray(json_decode(trim(file_get_contents($this->file)), true) ?? []);
        }
        return $this;
    }

    public function getContent(): NativeArray
    {
        return $this->read()->content;
    }
}