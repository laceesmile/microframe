<?php

declare(strict_types=1);

namespace Microframe\File\Exception;

use Microframe\Common\Exception\MicroframeException;

class PathNotFoundException extends MicroframeException
{
    public function __construct(string $path, int $code = 400)
    {
        $this->code = $code;
        $this->message = $path . " not found. Type 'true' to moveTo second parameter to create path.";
    }
}