<?php

declare(strict_types=1);

namespace Microframe\File\Exception;

use Microframe\Common\Exception\MicroframeException;

class PathCreatePermissionException extends MicroframeException
{
    public function __construct(string $path, int $code = 400)
    {
        $this->code = $code;
        $this->message = $path . " cannot create. Please check permissions.";
    }
}