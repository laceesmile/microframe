<?php

declare(strict_types=1);

namespace Microframe\File;

interface FileInterface
{
    public function getName();

    public function getExtension();

    public function getSize();

    public function rename(string $nameTo);

    public function moveTo(string $path);

    public function read();
}