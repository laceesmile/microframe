<?php

declare(strict_types=1);

namespace Microframe\File;

use Microframe\Common\Exception\FileNotFoundException;
use Microframe\Common\NativeArray;
use Microframe\File\Exception\PathCreatePermissionException;
use Microframe\File\FileInterface;

abstract class AbstractFile implements FileInterface
{
    private const DIR_SEP = DIRECTORY_SEPARATOR;

    protected string $file;
    protected string $name;
    protected int $size;
    protected string $extension;
    protected ?NativeArray $content = null;
    protected string $place;
    protected $error;
    protected string $fullName;
    protected $mimeType;

    public function __construct(string $file = null)
    {
        if ($file === null) {
            return;
        }
        if (!file_exists($file)) {
            throw new FileNotFoundException($file);
        }
        $this->build($file);
    }
    
    abstract public function getContent();

    public function buildFileFromRequest($file)
    {
        $this->build($file["tmp_name"]);
        $fileParams = pathinfo($file["name"]);
        $this->extension = $fileParams["extension"];
        $this->name = $fileParams["filename"];
        $this->fullName = $fileParams["filename"] . "." . $this->extension;

        return $this;
    }

    public function from($content, string $extension): File
    {
        if (is_resource($content)) {
            $content = stream_get_contents($content);
        }
        $name = uniqid();
        file_put_contents($name . "." . $extension, $content);

        return new static($name . "." . $extension);
    }

    protected function build(string $file)
    {
        $this->file = $file;
        $fileParams = pathinfo($file);
        $this->name = $fileParams["filename"];
        $this->size = filesize($file); //in bytes
        $this->extension = $fileParams["extension"] ?? null;
        $this->place = $fileParams["dirname"];
        $this->fullName = $fileParams["basename"];
        $this->mimeType = mime_content_type($this->file);
    }

    public function getName()
    {
        return $this->name;
    }

    public function getExtension(): string
    {
        return $this->extension;
    }

    public function getSizeRaw(): int
    {
        return $this->size;
    }

    public function getSize(int $decimals = 2): string
    {
        $factor = floor((strlen((string) $this->size) - 1) / 3);

        $sz = ['K', 'M', 'G', 'T', 'P'];
        $unit = null;

        if ($factor > 0 and isset($sz[$factor - 1])) {
            $unit = $sz[$factor - 1];
        }

        return sprintf("%.{$decimals}f", $this->size / pow(1024, $factor)) . $unit . 'B';
    }

    public function rename(string $nameTo)
    {
        rename($this->file, $this->place . self::DIR_SEP . $nameTo . "." . $this->extension);
        $this->name = $nameTo;
        $this->file = $this->place . self::DIR_SEP . $nameTo . "." . $this->extension;
        $this->fullName = $nameTo . "." . $this->extension;
    }

    public function moveTo(string $path, bool $createPath = true)
    {
        if ($createPath) {
            $this->createPath($path);
        }

        rename($this->file, $path . self::DIR_SEP . $this->name . "." . $this->extension);
        $this->place = $path;
        $this->file = $this->place . self::DIR_SEP . $this->name . "." . $this->extension;
    }

    public function createPath(string $path, int $permission = 0775)
    {
        self::createPathStatic($path, $permission);
    }

    public static function createPathStatic(string $path, int $permission = 0775)
    {
        if (!file_exists($path) and !mkdir($path, $permission, true)) {
            throw new PathCreatePermissionException($path);
        }
    }

    /**
     * Get the value of fullName
     */ 
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * Get the value of file
     */ 
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get the value of place
     */ 
    public function getPlace()
    {
        return $this->place;
    }

    public function getMimeType()
    {
        return $this->mimeType;
    }
}