<?php

declare(strict_types=1);

namespace Microframe\Converter;

use Microframe\Converter\ConverterInterface;
use Microframe\DI;
use Microframe\Helper\RequiredArgsConstructor;
use Microframe\Validable\Exception\InvalidRequestException;
use Microframe\Validable\RequestValidator;
use ReflectionClass;
use ReflectionEnum;
use ReflectionProperty;
use TypeError;

abstract class AbstractConverter extends DI implements ConverterInterface
{
    use RequiredArgsConstructor;
    
    protected static string $sourceClass;
    protected static string $targetClass;

    /**
     * @inheritDoc
     */
    public function convert($source, string $targetClass)
    {
        $sourceClass = static::$sourceClass;
        if (gettype($source) !== $sourceClass and !($source instanceof $sourceClass)) {
            throw new TypeError("Conversion source object must be " . $sourceClass, 500);
        }

        $target = new $targetClass();

        $sourceArray = is_array($source) ? $source : $source->toArray();

        $propertyList = (new ReflectionClass($targetClass))
            ->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

        foreach ($propertyList as $property) {
            $name = $property->getName();
            if (isset($sourceArray[$name])) {
                $propertyType = $property->getType()->getName();
                if ($this->isEnum($propertyType)) {
                    $target->$name = $this->convertToEnum($propertyType, $sourceArray[$name], $name);
                    continue;
                }
                $target->$name = $sourceArray[$name];
            }
        }
        if ($target instanceof RequestValidator) {
            $target->clean();
        }
        return $target;
    }

    public static function getSourceClass(): string
    {
        return static::$sourceClass;
    }

    public static function getTargetClass(): string
    {
        return static::$targetClass;
    }

    private function isEnum($propertyType): bool
    {
        return enum_exists($propertyType);
    }

    private function convertToEnum(string $enumClass, $source, string $name): mixed
    {
        $type = (new ReflectionEnum($enumClass))->getBackingType()?->getName();
        settype($source, $type);
        $value = $enumClass::tryFrom($source);
        if (!$value) {
            throw new InvalidRequestException(sprintf('Invalid value given to %s', $name), 400);
        }

        return $value;
    }
}