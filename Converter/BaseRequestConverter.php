<?php

declare(strict_types=1);

namespace Microframe\Converter;

use Microframe\Converter\AbstractConverter;

class BaseRequestConverter extends AbstractConverter
{
    protected static string $sourceClass = "array";
    protected static string $targetClass;

    public function __construct(string $targetClass)
    {
        static::$targetClass = $targetClass;
    }
}