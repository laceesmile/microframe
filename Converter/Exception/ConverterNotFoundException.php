<?php

declare(strict_types=1);

namespace Microframe\Converter\Exception;

use Microframe\Common\Exception\MicroframeException;

class ConverterNotFoundException extends MicroframeException
{
    public function __construct(string $sourceClass, string $targetClass, int $code = 500)
    {
        $this->code = $code;
        $this->message = "No capable converter found from [" . $sourceClass . "] to [" . $targetClass . "]";
    }
}