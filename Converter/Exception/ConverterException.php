<?php

declare(strict_types=1);

namespace Microframe\Converter\Exception;

use Microframe\Common\Exception\MicroframeException;

class ConverterException extends MicroframeException
{
    public function __construct(string $sourceType, string $targetType, string $field, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Can't convert " . $sourceType . " to " . $targetType . ". Value name: " . $field . ".";
    }
}