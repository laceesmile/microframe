<?php

declare(strict_types=1);

namespace Microframe\Converter;

interface ConverterInterface
{
    /**
     * The convertation method.
     * Must return the converted object
     *
     * @param $sourceObject The source object
     * @param string $targetType target class name
     * @return any The target object
     */
    public function convert($sourceObject, string $targetType);
}
