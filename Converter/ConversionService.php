<?php

declare(strict_types=1);

namespace Microframe\Converter;

use Illuminate\Support\Collection;
use Microframe\Common\NativeArray;
use Microframe\Common\RegisterTrait;
use Microframe\Converter\AbstractConverter;
use Microframe\Converter\BaseRequestConverter;
use Microframe\Converter\Exception\ConverterNotFoundException;
use Microframe\Helper\Helper;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Validable\RequestValidator;
use TypeError;

/**
 * @method void addConverter() addConverter(AbstractConverter $converter) Add a converter
 */
class ConversionService implements RegistableInterface
{
    use Helper, RegisterTrait;
    
    private NativeArray $converters;

    public function __construct()
    {
        $this->converters = new NativeArray();
    }

    public function addConverter(string $converter)
    {
        if (!is_subclass_of($converter, AbstractConverter::class)) {
            throw new TypeError("Converter must be instance of AbstractConverter", 500);
        }

        $this->converters[$converter::getSourceClass()][$converter::getTargetClass()] = $converter;
    }

    public function addConverters(array $converters)
    {
        foreach ($converters as $converter) {
            $this->addConverter($converter);
        }
    }

    private function getConverter(string $sourceClass, string $targetClass): AbstractConverter
    {
        $converter = $this->converters[$sourceClass][$targetClass] ?? null;
        if (!$converter) {
            throw new ConverterNotFoundException($sourceClass, $targetClass);
        }
        return new $converter();
    }

    public function convert($sourceObject, string $targetClass): object
    {
        $type = gettype($sourceObject) !== 'object' ? gettype($sourceObject) : get_class($sourceObject);
        if (!$this->hasConverter($type, $targetClass) and is_array($sourceObject) and is_subclass_of($targetClass, RequestValidator::class)) {
            return (new BaseRequestConverter($targetClass))->convert($sourceObject, $targetClass);
        }
        return $this->getConverter($type, $targetClass)->convert($sourceObject, $targetClass);
    }

    public function convertAll(array|NativeArray|Collection $sourceObjects, string $targetClass): array
    {
        $result = [];
        $sourceObjects = $this->getArray($sourceObjects);
        if (empty($sourceObjects)) {
            return $sourceObjects;
        }
        $sourceObject = reset($sourceObjects);
        $type = gettype($sourceObject) !== 'object' ? gettype($sourceObject) : get_class($sourceObject);
        $converter = $this->getConverter($type, $targetClass);
        foreach ($sourceObjects as $sourceObject) {
            $result[] = $converter->convert($sourceObject, $targetClass);
        }
        return $result;
    }

    public function hasConverter(string $sourceClass, string $targetClass): bool
    {
        return isset($this->converters[$sourceClass][$targetClass]);
    }
}