<?php

declare(strict_types=1);

namespace Microframe;

use Microframe\Di\DiObject;
use Microframe\Di\DiStorage;
use Microframe\Interfaces\RegistableInterface;

class DI
{
    public const BY_TYPES_KEY = 'byTypes';

    public function set(string $name, $value, bool $shared = true, string $className = null): self
    {
        DiStorage::set($name, (new DiObject())->build($value, $shared));
        DiStorage::get(self::BY_TYPES_KEY)->getValue()->$name = $className ?? get_class($value);

        return $this;
    }

    public function add(string $name, $value, bool $shared = true)
    {
        return $this->set($name, $value, $shared);
    }

    public function get(string $name, $params = [], bool $run = true)
    {
        if (!is_array($params)) {
            $params = [$params];
        }

        $diObject = DiStorage::get($name) ?? new DiObject();
        
        return $this->getProperObject($diObject->getValue($params), $run, $params);
    }

    public function __get(string $name)
    {
        return $this->get($name, [], false);
    }

    private function getProperObject($object, bool $run = false, array $params = [])
    {
        return ($object instanceof \Closure and $run) ? $object(...$params) : $object;
    }

    public function destroy(string $name)
    {
        DiStorage::remove($name);
    }

    public function has(string $name): bool
    {
        return DiStorage::has($name);
    }

    public function getByType(string $name): mixed
    {
        $list =$this->get(self::BY_TYPES_KEY);
        foreach ($list as $diName => $className) {
            if ($className === $name) {
                return $this->get($diName);
            }
        }
        return null;
    }

    public function getAll()
    {
        return DiStorage::getStorage();
    }

    public function register(RegistableInterface|string $object, $constructParams = [], bool $shared = true): self
    {
        if (is_string($object) and is_subclass_of($object, RegistableInterface::class)) {
            $constructParams = is_array($constructParams) ? $constructParams : [$constructParams];
            return $this->set($object::getRegisterName(), static function() use($object, $constructParams) {
                return new $object(...$constructParams);
            }, $shared, $object);
        } else {
            return $this->set($object::getRegisterName(), $object, $shared);
        }
    }
}
