<?php

declare(strict_types=1);

namespace Microframe\Storage;

use Microframe\Common\NativeArray;
use Microframe\Interfaces\StorageInterface;

class RuntimeMemory implements StorageInterface
{
    private static ?NativeArray $storage = null;

    public static function getStorage(): NativeArray
    {
        if (self::$storage === null) {
            self::$storage = new NativeArray();
        }
        return self::$storage;
    }

    public static function get(string $name): mixed
    {
        return self::getStorage()->$name ?? null;
    }

    public static function set(string $name, $value): void
    {
        self::getStorage()->$name = $value;
    }

    public static function has(string $name): bool
    {
        return self::getStorage()->$name !== null;
    }

    public static function remove(string $name): void
    {
        unset(self::getStorage()->$name);
    }

    public static function clear()
    {
        self::$storage = null;
    }
}