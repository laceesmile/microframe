<?php

declare(strict_types=1);

namespace Microframe\Proxy\Exception;

use Microframe\Common\Exception\MicroframeException;

class ControllerNotFoundException extends MicroframeException
{
    public function __construct(string $filename, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Controller {$filename} not found";
    }
}