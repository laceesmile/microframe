<?php

declare(strict_types=1);

namespace Microframe\Proxy;

use ReflectionClass;

class ObjectProxy extends AbstractProxy
{
    /**
     * Get the object property list.
     *
     * @return array
     */
    public function getProperties(): array
    {
        return $this->getObjectReflection()->getProperties();
    }
    /**
     * Get the object reflection.
     *
     * @return ReflectionClass
     */
    protected function getObjectReflection(): ReflectionClass
    {
        return new ReflectionClass($this->className);
    }

    protected function process(): void
    {
        foreach ($this->getProperties() as $property) {
            $this->values[$property->getName()] = $this->convertIfNeed($property);
        }
    }
}