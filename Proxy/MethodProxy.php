<?php

declare(strict_types=1);

namespace Microframe\Proxy;

use Microframe\Proxy\Exception\ControllerNotFoundException;
use Microframe\Proxy\Exception\MethodNotFoundException;
use ReflectionMethod;

class MethodProxy extends AbstractProxy
{
    /**
     * Name of the method.
     *
     * @var string
     */
    private string $methodName;

    /**
     * Constructor.
     *
     * @param string $className
     * @param string $methodName
     */
    public function __construct(string $className, string $methodName, array $source = [])
    {
        parent::__construct($className, $source);
        $this->methodName = $methodName;
    }

    /**
     * @inheritDoc
     */
    public function process(): void
    {
        $function = $this->getMethodReflection();
        $parameters = $function->getParameters();
        foreach ($parameters as $parameter) {
            $this->values[$parameter->getName()] = $this->convertIfNeed($parameter);
        }
    }

    /**
     * Get the method reflection.
     *
     * @return ReflectionMethod
     */
    private function getMethodReflection(): ReflectionMethod
    {
        if (!class_exists($this->className)) {
            throw new ControllerNotFoundException($this->className);
        }

        if (!method_exists($this->className, $this->methodName)) {
            throw new MethodNotFoundException($this->className, $this->methodName);
        }
        return new ReflectionMethod($this->className, $this->methodName);
    }
}