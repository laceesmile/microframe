<?php

declare(strict_types=1);

namespace Microframe\Proxy;

use Microframe\Converter\ConversionService;
use Microframe\DI;
use ReflectionParameter;
use ReflectionProperty;

abstract class AbstractProxy {
    /**
     * The object what referenced.
     *
     * @var string
     */
    protected string $className;

    /**
     * Source of the values.
     * These converted to the required types.
     * 
     * @var array
     */
    protected $source = [];

    /**
     * Conversion service.
     * 
     * @var ConversionService
     */
    protected ?ConversionService $converters;

    /**
     * Processed values.
     * 
     * @var array
     */
    protected array $values = [];

    /**
     * Method paramter name to type.
     * 
     * @var array
     */
    protected array $types = [];

    /**
     * Constructor.
     *
     * @param string $className
     * @param array $source 
     */
    public function __construct(string $className, array $source = [])
    {
        $this->className = $className;
        $this->source = $source;

        $this->conversionService = (new DI())->get("conversionService");
    }

    /**
     * Set the values as associative array.
     *
     * @return void
     */
    abstract protected function process(): void;

    /**
     * Get the compiled values as list.
     * Use it with ellipsis operator.
     *
     * @return array
     */
    public function get(): array
    {
        return array_values($this->getAssoc());
    }

    /**
     * Get the compiled parameters as associative array.
     *
     * @return array
     */
    public function getAssoc(): array
    {
        if (empty($this->values)) {
            $this->process();
        }

        return $this->values;
    }

    /**
     * Set the source values.
     *
     * @return self
     */
    public function setSource(array $source): self
    {
        $this->source = $source;

        return $this;
    }

    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * Convert a parameter to type if need.
     *
     * @param ReflectionParameter|ReflectionProperty $parameter
     * @return null|value converted to type
     */
    protected function convertIfNeed(ReflectionParameter|ReflectionProperty $parameter): mixed
    {
        if (!$parameter->hasType()) {
            return null;
        }
        $name = $parameter->getName();
        $type = $parameter->getType()->getName();
        $this->types[$name] = $type;
        if (class_exists($type)) {
            return $this->conversionService->convert($this->source, $type);
        }
        if (!isset($this->source[$name])) {
            return null;
        }
        $value = $this->source[$name];
        settype($value, $type);
        return $value;
    }
}