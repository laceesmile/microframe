<?php

declare(strict_types=1);

namespace Microframe\Event;

use Illuminate\Contracts\Events\Dispatcher;
use Microframe\Common\RegisterTrait;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Plugin\Exception\PluginException;
use Microframe\Plugin\PluginContainer;

class EventHandler implements Dispatcher, RegistableInterface
{
    use RegisterTrait;
    private PluginContainer $container;

    public function __construct(PluginContainer $container = null)
    {
        $this->container = $container ?? new PluginContainer();
    }

    public function __call(string $method, array $parameters = [])
    {
        if ($this->container->$method) {
            if (is_array($this->container->$method)) {
                foreach ($this->container->$method as $plugin) {
                    if (($response = $this->run($plugin, $method, $parameters)) !== null) {
                        return $response;
                    }
                }
            } else {
                return $this->run($this->container->$method, $method, $parameters);
            }
        }
    }

    public function listen($events, $plugin = null)
    {
        foreach ((array) $events as $eventName) {
            $this->add($eventName, $plugin);
        }
    }

    public function hasListeners($eventName)
    {
        return $this->container->has($eventName);
    }

    public function subscribe($subscriber)
    {
        
    }
    
    public function until($event, $payload = [])
    {
        return $this->dispatch($event, $payload);
    }

    public function forgetPushed()
    {

    }

    public function dispatch($event, $parameters = [], $halt = false)
    {
        $plugin = $this->container->get($event);
        if (!$plugin) {
            return;
        }
        return $this->run($plugin, $event, $parameters);
    }

    public function flush($event)
    {
        $this->container->clear();
    }

    public function forget($event)
    {
        $this->container->remove($event);

        return $this;
    }
    
    public function push($event, $payload = null): self
    {
        return $this->add($event, $payload);
    }

    public function add($name, $plugin = null): self
    {
        if (is_array($name)) {
            foreach ($name as $functionName) {
                $this->container->add($functionName, $plugin);
            }
        } else {
            $this->container->add($name, $plugin);
        }
        
        return $this;
    }

    private function run($plugin, string $method, $parameters = null)
    {
        if (is_array($plugin)) {
            foreach ($plugin as $p) {
                if (($response = $this->handle($p, $method, $parameters)) !== null) {
                    return $response;
                }
            }
        } else {
            return $this->handle($plugin, $method, $parameters);
        }
    }

    private function handle($plugin, $method, $parameters = [])
    {
        if (is_string($plugin)) {
            list($plugin, $method) = explode("@", $plugin);
            $plugin = new $plugin();
        }
        if (!method_exists($plugin, $method)) {
            throw new PluginException("{$method} method does not exist on " . get_class($plugin), 405);
        }
        return $plugin->$method($parameters);
    }

}