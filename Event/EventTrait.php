<?php

declare(strict_types=1);

namespace Microframe\Event;

use Microframe\DI;

trait EventTrait
{
    private function runEvent(string $method, array $parameters = [])
    {
        $di = new DI();
        if ($di->has("eventHandler")) {
            return $di->eventHandler->dispatch($method, ...$parameters);
        }
    }
}
