<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Common\NativeArray;
use Microframe\File\File as FileObject;
use Microframe\Validator\MimeValidator;
use Microframe\Form\Element\AbstractElement;

class File extends Validator
{
    const MAX_SIZE = "maxSize";
    const MAX_SIZE_MESSAGE = "maxSizeMessage";

    const MIN_SIZE = "minSize";
    const MIN_SIZE_MESSAGE = "minSizeMessage";
    
    const TYPE = "type";
    const TYPE_MESSAGE = "typeMessage";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (!(new Required($this->options))->validate($element)) {
            return false;
        }

        $file = $element->getValue();

        if ($file instanceof NativeArray) {
            foreach ($file as $fileObject) {
                $this->check($fileObject, $element);
            }
        } else {
            $this->check($file, $element);
        }
        
        return true;
    }

    private function check(FileObject $file, AbstractElement $element): bool
    {
        if (!$this->checkSize($file, $element)) {
            return false;
        }
        if (!$this->checkType($file, $element)) {
            return false;
        }
        return true;
    } 

    private function checkType(FileObject $file, AbstractElement $element): bool
    {
        if ($this->getOption(self::TYPE)) {
            $type = mime_content_type($file->getFile());
            if (!MimeValidator::typeCheck($this->getOption(self::TYPE), $type)) {
                $message = $this->getOption(self::TYPE_MESSAGE) ?? "Not allowed file type!";
                $errorClass = $this->config->errorClass;
                $element->setMessage(new $errorClass($message));
                return false;
            }
        }
        return true;
    }


    private function checkSize(FileObject $file, AbstractElement $element): bool
    {
        $sizeString = $file->getSize();
        $sizeInBytes = $this->sizeFilter(mb_substr($sizeString, 0, -1)); // for remove B

        if ($this->getOption(self::MAX_SIZE)) {
            $max = $this->sizeFilter($this->getOption(self::MAX_SIZE));
            if ($max < $sizeInBytes) {
                $message = $this->getOption(self::MAX_SIZE_MESSAGE) ?? "Maximum file size should be under " . self::MAX_SIZE . "B.";
                $errorClass = $this->config->errorClass;
                $element->setMessage(new $errorClass($message));
                return false;
            }
        }

        if ($this->getOption(self::MIN_SIZE)) {
            $min = $this->sizeFilter($this->getOption(self::MIN_SIZE));
            if ($min > $sizeInBytes) {
                $message = $this->getOption(self::MIN_SIZE_MESSAGE) ?? "Minimum file size should be more than " . self::MIN_SIZE . "B.";
                $errorClass = $this->config->errorClass;
                $element->setMessage(new $errorClass($message));
                return false;
            }
        }
        return true;
    }

    private function sizeFilter(string $size): int
    {
        $unit = strtoupper(substr($size, -1));
        $size = (float) mb_substr($size, 0, -1);

        $unitArray = array('K', 'M', 'G', 'T', 'P');

        $exp = array_search($unit, $unitArray);
        $bytes = $size * pow(1024, $exp + 1);
        return (int) $bytes;
    }
}