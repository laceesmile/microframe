<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Exception\MissingParameterException;
use Microframe\Validator\Validator;

class Length extends Validator
{
    private const LENGTH = "length";

    protected string $message = "%name% should be exactly %length% character length!";

    public function validate(?string $value): bool
    {
        if (($this->getOption(self::LENGTH)) == null ) {
            throw new MissingParameterException(self::LENGTH);
        }
        
        if (strlen($value) == $this->getOption(self::LENGTH)) {
            return true;
        }
        return false;
    }

	public function getMessageParams(): array
    {
        return [self::LENGTH => self::getOption(self::LENGTH)];
	}
}