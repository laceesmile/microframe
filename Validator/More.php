<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class More extends Validator
{
    const MIN_VALUE = "minValue";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::MIN_VALUE)) == null ) {
            throw new MissingParameterException(self::MIN_VALUE);
        }

        $value = $element->getValue();
        if ($value < $this->getOption(self::MIN_VALUE)) {
            $message = $this->getOption(self::MESSAGE) ?? strtolower($element->label()->toString() ?? $element->getName()) . " should be more than " . $this->getOption(self::MIN_VALUE) . "!";
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($message));
            return false;
        }
        return true;
    }
}
