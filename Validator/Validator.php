<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Common\NativeArray;
use Microframe\DI;
use Microframe\Form\Element\AbstractElement;
use Microframe\Helper\Helper;
use Microframe\Html\Message;
use Microframe\Translator\Translator;
use Microframe\Validable\Validable;
use Microframe\Validator\ValidatorInterface;

abstract class Validator implements ValidatorInterface
{
    use Helper;
    
    const ALLOW_EMPTY = "allowEmpty";
    const MESSAGE = "message";
    const EMPTY_MESSAGE = "emptyMessage";
    const VALUE = "value";
    const ERROR_CLASS = "errorClass";

    protected NativeArray $config;
    protected array $options = [];
    protected string $emptyMessage = "%name% field is required";

    protected string $message;

    private Translator $trans;
    
    public function __construct(array $options = [])
    {
        $this->options = $options;
        
        $di = new DI();
        $this->trans = $di->has("translator") ? $di->get("translator") : new Translator();
        $config = $di->get("microframeConfig") ?? new NativeArray();
        $this->config = new NativeArray(array_merge($this->getArray($this->getDefaultConfig()), $this->getArray($config) ?? []));


        $this->config->errorClass = $this->options[self::ERROR_CLASS] ?? $this->config->{self::ERROR_CLASS};
    }

    public function checkAllowEmpty(Validable $validable): bool
    {
        if ($this->emptyValueEnabled()) {
            return true;
        }
        return !$this->isEmpty($validable->getValue());
    }

    public function emptyValueEnabled(): bool
    {
        return ($this->getOption(self::ALLOW_EMPTY) ?? true and $this->getOption(self::ALLOW_EMPTY) ?? true === true);
    }

    public function isEmpty($value): bool
    {
        if ($value instanceof NativeArray) {
            $value = $value->toArray();
        }
        return (empty($value) or $value == "" or $value == null) and (filter_var($value, FILTER_VALIDATE_INT) !== (int) $value);
    }


    public function getMessage(bool $isEmpty = true): string
    {
        return $isEmpty ?
            $this->getOption(self::EMPTY_MESSAGE) ?? $this->emptyMessage :
            $this->getOption(self::MESSAGE) ?? $this->message;
    }

    public function getConfig(): NativeArray
    {
        return $this->config;
    }

    protected function getOption(string $optionName)
    {
        return $this->options[$optionName] ?? null;
    }

    protected function getOptions(): array
    {
        return $this->options ?? [];
    }

    private function getDefaultConfig(): NativeArray
    {
        return new NativeArray([
            "charset" => mb_internal_encoding(),
            self::ERROR_CLASS => Message::class
        ]);
    }
    
    public function __serialize(): array
    {
        return $this->options;
    }

    public function unserialize($serialized): void
    {
        $this->options = unserialize($serialized);
    }

    public function jsonSerialize(): mixed
    {
        return $this->options;
    }

    public function getMessageParams(): array
    {
        return [];
    }
}