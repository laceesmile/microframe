<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;


class Required extends Validator
{
    public function validate($elementValue): bool
    {
        return !$this->isEmpty($elementValue) and (is_scalar($elementValue) and $this->fullfilled($elementValue));
    }
}