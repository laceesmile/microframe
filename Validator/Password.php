<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Password extends Validator
{
    const DEFAULT_INVALID_MESSAGE = "Invalid password";

    const RULES = [
        "minLength",
        "maxLength",
        "uppercase",
        "lowercase",
        "number",
        "special",
    ];

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        $called = array_map(function($item){return strtolower($item);}, array_keys($this->getOptions()));
        
        foreach (self::RULES as $name) {
            $availableRules[$name] = strtolower($name);
        }
        $callable = [
            "empty",
        ];

        foreach ($called as $cal) {
            if (in_array($cal, $availableRules)) {
                $callable[] = array_search($cal, $availableRules);
            }
        }

        foreach ($callable as $ruleName) {
            if (!$this->handle($element, $ruleName)) {
                return false;
            }
        }
        return true;
    }

    private function handle(AbstractElement $element, $parameter)
    {
        $elementValue = (string) $element->getValue();

        $option = $this->getOption($parameter);
        $validatorValue = $option;
        $validatorMessage = $this->getOption(self::MESSAGE) ?? $this->getOption($parameter . ucfirst(self::MESSAGE)) ?? self::DEFAULT_INVALID_MESSAGE;

        if (is_array($option)) {
            if (!isset($option[self::VALUE])) {
                throw new MissingParameterException(self::VALUE);
            }
            $validatorValue = $option[self::VALUE];
            $validatorMessage = $option[self::MESSAGE] ?? $validatorMessage;
        }

        $func = "check" . ucfirst($parameter);
        if (!$this->$func($elementValue, $validatorValue)) {
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($validatorMessage));
            return false;
        }

        return true;
    }

    private function checkEmpty(?string $value)
    {
        return !$this->isEmpty($value);
    }

    private function checkMinLength(string $password, int $length = 8): bool
    {
        return mb_strlen($password, $this->config->charset) >= $length;
    }

    private function checkUpperCase(string $password): bool
    {
        // Is there any capital letter
        return (bool) preg_match('/[A-Z]/', $password);
    }

    private function checkLowerCase(string $password): bool
    {
        // Is there any capital letter
        return (bool) preg_match('/[a-z]/', $password);
    }

    private function checkMaxLength(string $password, int $length): bool
    {
        // is short enough
        return mb_strlen($password, $this->config->charset) <= $length;
    }

    private function checkNumber(string $password): bool
    {
        // Is there any numeric charater
        return (bool) preg_match('/[0-9]/', $password);
    }

    private function checkSpecial(string $password, string $specialList = "!@#$%^&*?_~-ÃÂ£():{}|<>"): bool
    {
        // Is there any special character
        return (bool) preg_match('/[' . $specialList . ']/', $password);
    }
}
