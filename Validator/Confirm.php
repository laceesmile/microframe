<?php

declare(strict_types=1);

namespace Microframe\Validator;

use InvalidArgumentException;
use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Confirm extends Validator
{
    const CONFIRM = "confirm";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::CONFIRM)) == null ) {
            throw new MissingParameterException(self::CONFIRM);
        }

        $confirmElement = $this->getOption(self::CONFIRM);
        if (!($confirmElement instanceof AbstractElement)) {
            throw new InvalidArgumentException(self::class . " " . self::CONFIRM . " should be an instance of " . AbstractElement::class);
        }

        if ($element->getValue() !== $confirmElement->getValue()) {
            $message = $this->getOption(self::MESSAGE) ?? strtolower($element->label()->toString() ?? $element->getName()) . " should be same as " . $confirmElement->getName() . "!";
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($message));
            return false;
        }

        return true;

    }
}