<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\FileMimeType\MimeDoc;
use Microframe\Validator\FileMimeType\MimePdf;
use Microframe\Validator\FileMimeType\MimeAudio;
use Microframe\Validator\FileMimeType\MimeImage;
use Microframe\Validator\FileMimeType\MimeSheet;
use Microframe\Validator\FileMimeType\MimeVideo;
use Microframe\Validator\FileMimeType\MimePresentation;

class MimeValidator
{
    const MIME_FILE_LIST = [
        "image" => MimeImage::class,
        "doc" => MimeDoc::class,
        "sheet" => MimeSheet::class,
        "audio" => MimeAudio::class,
        "video" => MimeVideo::class,
        "presentation" => MimePresentation::class,
        "pdf" => MimePdf::class,
    ];

    public static function typeCheck($allowedType, string $fileMimeType): bool
    {
        if (is_array($allowedType)) {
            foreach ($allowedType as $allowed) {
                if (self::check($allowed, $fileMimeType)) {
                    return true;
                }
            }
            return false;
        }
        return self::check($allowedType, $fileMimeType);
    }

    private static function check(string $allowedType, string $fileMimeType): bool
    {
        $type = explode("/", $allowedType)[0];
        $typeList = self::MIME_FILE_LIST[$type];
        return in_array($fileMimeType, $typeList::TYPE_LIST);
    }
}