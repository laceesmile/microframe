<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Helper\Helper;
use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Exclusion extends Validator
{
    use Helper;
    const LIST = "list";
    const KEY_VALUE_PAIR = "keyValuePair";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::LIST)) == null ) {
            throw new MissingParameterException(self::LIST);
        }

        $list = $this->getOption(self::LIST);
        if (!is_array($list)) {
            $list = $this->getArray($list);
        }

        $list = $this->getOption(self::KEY_VALUE_PAIR) === true ? array_keys($list) : $list;

        $value = $element->getValue();
        if (!in_array($value, $list)) {
            return true;
        }

        $message = $this->getOption(self::MESSAGE) ?? "Please select from the list!";
        $errorClass = $this->config->errorClass;
        $element->setMessage(new $errorClass($message));
        return false;
    }
}
