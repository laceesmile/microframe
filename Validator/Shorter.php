<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Shorter extends Validator
{
    const MAX_LENGTH = "length";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::MAX_LENGTH)) == null ) {
            throw new MissingParameterException(self::MAX_LENGTH);
        }
        
        $value = $element->getValue();
        if (strlen($value) >= $this->getOption(self::MAX_LENGTH)) {

            $message = $this->getOption(self::MESSAGE) ?? strtolower($element->label()->toString() ?? $element->getName()) . " should be shorter than " . $this->getOption(self::MAX_LENGTH) . " characters!";
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($message));

            return false;
        }
        return true;
    }
}