<?php

declare(strict_types=1);

namespace Microframe\Validator\Exception;

use Microframe\Common\Exception\MicroframeException;

class MissingParameterException extends MicroframeException
{
    public function __construct(string $parameterName, int $code = 404)
    {
        $this->code = $code;
        $this->message = "Missing " . $parameterName . " parameter from validator parameters!";
    }
}