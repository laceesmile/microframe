<?php

declare(strict_types=1);

namespace Microframe\Validator\Exception;

use Microframe\Common\Exception\MicroframeException;

class NotCallableException extends MicroframeException
{
    public function __construct($parameter, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Validator $parameter is not callable!";
    }
}