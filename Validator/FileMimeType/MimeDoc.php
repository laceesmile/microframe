<?php

declare(strict_types=1);

namespace Microframe\Validator\FileMimeType;

use Microframe\Validator\MimeValidator;

class MimeDoc extends MimeValidator
{
    const TYPE = "doc/*";

    const TYPE_LIST = [
        "application/doc",
        "application/ms-doc",
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/rtf",
        "text/plain"
    ];
}
