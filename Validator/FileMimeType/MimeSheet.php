<?php

declare(strict_types=1);

namespace Microframe\Validator\FileMimeType;

use Microframe\Validator\MimeValidator;

class MimeSheet extends MimeValidator
{
    const TYPE = "sheet/*";

    const TYPE_LIST = [
        "application/excel",
        "application/vnd.ms-excel",
        "application/x-excel",
        "application/x-msexcel",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "text/csv",
        "text/tsv",
        "application/vnd.oasis.opendocument.spreadsheet",
    ];
}
