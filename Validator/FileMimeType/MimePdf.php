<?php

declare(strict_types=1);

namespace Microframe\Validator\FileMimeType;

use Microframe\Validator\MimeValidator;

class MimePdf extends MimeValidator
{
    const TYPE = "application/pdf";

    const TYPE_LIST = [
        "application/pdf"
    ];
}
