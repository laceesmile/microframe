<?php

declare(strict_types=1);

namespace Microframe\Validator\FileMimeType;

use Microframe\Validator\MimeValidator;

class MimePresentation extends MimeValidator
{
    const TYPE = "application/presentation";

    const TYPE_LIST = [
        "application/mspowerpoint",
        "application/powerpoint",
        "application/vnd.ms-powerpoint",
        "application/x-mspowerpoint",
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "application/vnd.oasis.opendocument.presentation",
    ];
}
