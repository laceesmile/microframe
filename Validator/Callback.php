<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;
use Microframe\Validator\Exception\NotCallableException;

class Callback extends Validator
{
    const CALLBACK = "function";
    const SEPARATE_MESSAGES = "separateMessages";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::CALLBACK)) == null ) {
            throw new MissingParameterException(self::CALLBACK);
        }

        if (($this->getOption(self::MESSAGE)) === null and !$this->getOption(self::SEPARATE_MESSAGES)) {
            throw new MissingParameterException(self::MESSAGE);
        }

        $closure = $this->getOption(self::CALLBACK);
        if (!is_callable($closure)) {
            throw new NotCallableException(self::CALLBACK);
        }
        $value = $element->getValue();

        $response = $closure($value, $element, $this);
        if ($response) {
            return true;
        }

        if ($this->getOption(self::SEPARATE_MESSAGES)) {
            return false;
        }
        
        $errorClass = $this->config->errorClass;
        $element->setMessage(new $errorClass($this->getOption(self::MESSAGE)));
        return false;
    }

    public function setMessage(string $message) {
        $this->options[self::MESSAGE] = $message;
    }
}
