<?php

declare(strict_types=1);

namespace Microframe\Validator;

use \JsonSerializable;

interface ValidatorInterface extends JsonSerializable
{
    public function __construct(array $options = []);

    public function validate(?string $element): bool;

    public function getMessageParams(): array;

}