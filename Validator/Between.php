<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Validator\Exception\MissingParameterException;

class Between extends Validator
{
    const MAX_VALUE = "max";
    const MIN_VALUE = "min";

    protected string $message = "%name% should be between %min% and %max%!";

    public function validate($value): bool
    {
        if (($this->getOption(self::MAX_VALUE)) == null ) {
            throw new MissingParameterException(self::MAX_VALUE);
        }
        if (($this->getOption(self::MIN_VALUE)) === null ) {
            throw new MissingParameterException(self::MIN_VALUE);
        }

        return $value <= $this->getOption(self::MAX_VALUE) and $value >= $this->getOption(self::MIN_VALUE);
    }

    public function getMessageParams(): array
    {
        return [
            self::MAX_VALUE => self::getOption(self::MAX_VALUE),
            self::MIN_VALUE => self::getOption(self::MIN_VALUE),
        ];
	}
}
