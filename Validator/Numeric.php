<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;

class Numeric extends Validator
{
    const ENABLED_FLOAT = "enabledFloat";

    protected string $message = "Field %name% must be numeric!";

    public function validate($elementValue): bool
    {
        $elementValue = $elementValue ?? '';
        return (((is_int($elementValue) or ctype_digit($elementValue) and $elementValue != "")) or ($this->getOption(self::ENABLED_FLOAT) === true and is_float(filter_var($elementValue, FILTER_VALIDATE_FLOAT))));
    }
}
