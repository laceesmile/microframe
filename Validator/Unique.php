<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Mvc\Model;
use InvalidArgumentException;
use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Unique extends Validator
{
    const MODEL = "model";
    const COLUMN = "column";
    const CONDITIONS = "conditions";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($model = $this->getOption(self::MODEL)) == null) {
            throw new MissingParameterException(self::MODEL);
        }
        
        if (is_string($model)) {
            $model = new $model;
        }

        if (!($model instanceof Model)) {
            throw new InvalidArgumentException(self::class . " " . self::MODEL . " should be an instance of " . Model::class);
        }

        $column = $this->getOption(self::COLUMN) ?? $element->getName();
        $conditions = $this->getOption(self::CONDITIONS) ?? null;

        $builder = $model::where($column, $element->getValue());
        if ($conditions) {
            $builder->where($conditions);
        }

        $result = $builder->get();
        if ($result->count() == 0) {
            return true;
        } else {
            $message = $this->getOption(self::MESSAGE) ?? strtolower($element->label()->toString() ?? $element->getName()) . " is already exists in model " . get_class($model) . ".";
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($message));
            return false;
        }

    }
}