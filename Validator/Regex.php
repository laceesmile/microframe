<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Regex extends Validator
{
    const REGEX = "regex";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::REGEX)) == null ) {
            throw new MissingParameterException(self::REGEX);
        }
        
        $value = $element->getValue();
        if (preg_match($this->getOption(self::REGEX), $value, $matches)) {
            return true;
        }
        $message = $this->getOption(self::MESSAGE) ?? "Field " . strtolower($element->label()->toString() ?? $element->getName()) . " must pass the rule!";
        
        $errorClass = $this->config->errorClass;
        $element->setMessage(new $errorClass($message));

        return false;
    }
}