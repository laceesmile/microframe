<?php

declare(strict_types=1);

namespace Microframe\Validator;

use DateTime;
use Microframe\Validator\Validator;

class DateTimeValidator extends Validator
{
    const DATE_FORMAT = 'dateFormat';
    const TIME_FORMAT = 'timeFormat';

    private string $dateFormat;

    protected string $message = "%name% got invalid date format! The requested format %format%";

    public function validate($value = null): bool
    {
        $this->dateFormat = $this->getOption(self::DATE_FORMAT) ?? $this->config->dateFormat ?? "Y-m-d";
        if ($this->getOption(self::TIME_FORMAT)) {
            $this->dateFormat .= ' ' . $this->getOption(self::TIME_FORMAT) ?? $this->config->timeFormat ?? "H:i:s";
        }

        $converted = DateTime::createFromFormat($this->dateFormat, $value);
        if (!$converted) {
            return false;
        }
        return $value == $converted->format($this->dateFormat);
    }

    public function getMessageParams(): array
    {

        return ['format' => $this->dateFormat];
	}
}
