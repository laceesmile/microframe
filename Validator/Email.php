<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;

class Email extends Validator
{
    protected string $message = "Invalid email format!";

    public function validate($elementValue): bool
    {
        return !!filter_var($elementValue, FILTER_VALIDATE_EMAIL);
    }
}