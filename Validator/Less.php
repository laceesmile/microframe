<?php

declare(strict_types=1);

namespace Microframe\Validator;

use Microframe\Validator\Validator;
use Microframe\Form\Element\AbstractElement;
use Microframe\Validator\Exception\MissingParameterException;

class Less extends Validator
{
    const MAX_VALUE = "maxValue";

    public function validate(AbstractElement $element): bool
    {
        if ($this->checkAllowEmpty($element) === true) {
            return true;
        }

        if (($this->getOption(self::MAX_VALUE)) == null ) {
            throw new MissingParameterException(self::MAX_VALUE);
        }

        $value = $element->getValue();
        if ($value > $this->getOption(self::MAX_VALUE)) {
            $message = $this->getOption(self::MESSAGE) ?? strtolower($element->label()->toString() ?? $element->getName()) . " should be less than " . $this->getOption(self::MAX_VALUE) . "!";
            $errorClass = $this->config->errorClass;
            $element->setMessage(new $errorClass($message));
            return false;
        }
        return true;
    }
}
