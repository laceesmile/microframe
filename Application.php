<?php

declare(strict_types=1);

namespace Microframe;

use Microframe\Application\Autowire;
use Microframe\Application\Dispatcher;
use Microframe\Common\Exception\MicroframeException;
use Microframe\Common\NativeArray;
use Microframe\Converter\ConversionService;
use Microframe\DI;
use Microframe\Http\Request;
use Microframe\Http\Response;
use Microframe\Http\Url;
use Microframe\Provider\Provider;
use Microframe\Router\Router;

abstract class Application extends DI
{
    protected Provider $provider;

    public function __construct()
    {
        $this->provider = new Provider;
        $this->set(DI::BY_TYPES_KEY, new NativeArray());
        $this->set(Provider::DI_KEY, new NativeArray());


        $this->register(new ConversionService());
        $this->register(new Autowire());
        $this->register(new Request());
        $this->register(new Response());
        $this->register(new Dispatcher());
        $this->register(new Url(), null, false);
        $this->register(new Router());
        $this->initialize();
    }

    abstract function initialize(): void;
    
    public function run()
    {
        try {
            $this->response->send($this->dispatcher->fire());
        } catch (MicroframeException $e) {
            $this->response->sendException($e);
        }
    }
}