<?php

declare(strict_types=1);

namespace Microframe\Common\Exception;

use Microframe\Common\Exception\MicroframeException;

class MethodNotFoundException extends MicroframeException
{
    public function __construct(string $filename, string $methodName, int $code = 500)
    {
        $this->code = $code;
        $this->message = "Method {$methodName} not found on controller {$filename}";
    }
}