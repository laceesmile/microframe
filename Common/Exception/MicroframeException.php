<?php
namespace Microframe\Common\Exception;

use Exception;

class MicroframeException extends Exception
{
    public function toArray(): array
    {
        return [
            "message" => $this->message,
            "code" => $this->code
        ];
    }
}

