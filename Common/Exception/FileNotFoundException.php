<?php

declare(strict_types=1);

namespace Microframe\Common\Exception;

use Microframe\Common\Exception\MicroframeException;

class FileNotFoundException extends MicroframeException
{
    public function __construct(string $filename, int $code = 404)
    {
        $this->code = $code;
        $this->message = "File {$filename} not found";
    }
}