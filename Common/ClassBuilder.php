<?php

declare(strict_types=1);

namespace Microframe\Common;

use Microframe\Common\NativeArray;

class ClassBuilder
{
    private array $varMap = [];
    private bool $removeOnAccess = false;

    public function __construct(array|NativeArray|null $properties = null)
    {
        if ($properties) {
            $this->set($properties);
        }
    }

    public function __get(string $name)
    {
        $value = $this->$name ?? null;
        if ($this->removeOnAccess) {
            $this->remove($name);
        }
            return $value;
    }

    public function __set(string $name, $value)
    {
        $lowerName = strtolower($name);

        $this->varMap[$name] = $lowerName;

        $this->$name = $value;

        return $this;
    }

    public function getAll(): array
    {
        $array = get_object_vars($this);
        unset($array["varMap"], $array["removeOnAccess"]);
        return $array;
    }

    public function set($parameter, $defaultValue = null): self
    {
        if (is_array($parameter) || $parameter instanceof NativeArray) {
            foreach ($parameter as $key => $value) {
                $this->$key = $value;
            }
        } else {
            $this->$parameter = $defaultValue;
        }

        return $this;
    }

    public function get(string $name, bool $caseSensitive = true)
    {
        if ($caseSensitive) {
            return $this->$name ?? null;
        }
        $key = $this->getKey($name);
        if ($key) {
            return $this->$key;
        }
        return null;
    }

    public function has(string $name, bool $caseSensitive = true): bool
    {
        if ($caseSensitive) {
            return isset($this->$name);
        }
        return (bool) $this->getKey($name);
    }

    public function getFirst(array $possibilities = [], bool $caseSensitive = true, $defaultValue = null)
    {
        $key = $this->getFirstExist($possibilities, $caseSensitive);
        if ($key) {
            return $this->get($key, $caseSensitive);
        }
        return $defaultValue;
    }

    public function getFirstExist(array $possibilities = [], bool $caseSensitive = true): ?string
    {
        foreach ($possibilities as $possibility) {
            $result = $this->get($possibility, $caseSensitive);
            if ($result) {
                return $possibility;
            }
        }
        return null;
    }

    public function remove(string $name): bool
    {
        if ($this->has($name)) {
            unset($this->{$this->getKey($this->$name)});
            return true;
        }
        return false;
    }

    public function removeOnAccess(bool $removeOnAccess = true): self
    {
        $this->removeOnAccess = $removeOnAccess;
        return $this;
    }

    private function getKey(string $key): ?string
    {
        $savedName = array_search(strtolower($key), $this->varMap);
        if (!$savedName) {
            return null;
        }
        return $savedName;
    }
}