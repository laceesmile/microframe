<?php

declare(strict_types=1);

namespace Microframe\Common;

use Microframe\Application\Dispatcher;


trait RegisterTrait
{
    /**
     * Get the service name.
     *
     * @return string
     */
    public static function getRegisterName(): string
    {
        $namespace = explode(Dispatcher::NAMESPACE_DELIMITER, self::class);
        return lcfirst(array_pop($namespace));
    }
}