<?php

declare(strict_types=1);

namespace Microframe\Common;

use Microframe\Common\Exception\MethodNotFoundException;

trait ForwardCall
{
    public function __call(string $method, array $params = []): mixed
    {
        if (method_exists($this, $method)) {
            return static::$method(...$params);
        }
        throw new MethodNotFoundException(get_class($this), $method);
    }
}