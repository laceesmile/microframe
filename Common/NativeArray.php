<?php

declare(strict_types=1);

namespace Microframe\Common;

use \ArrayObject;
use \JsonSerializable;

/**
 * Class which has array and stdClass access to
 */

class NativeArray extends ArrayObject implements JsonSerializable
{
    /**
     * Initialize a NativeArray
     *
     * @param array $data
     */
    public function __construct(array|NativeArray $data = [])
    {
        if (!is_array($data)) {
            if (method_exists($data, "toArray")) {
                $data = $data->toArray();
            } else {
                $data = (array) $data;
            }
        } else {
            $this->setData($data);
        }
    }

    /**
     * Set data to NativeArray recursively
     *
     * @param array $data
     * @return void
     */
    public function setData(array $data = []): void
    {
        foreach ($data as $key => $value) {
            $this->offsetSet($key, is_array($value) ? new static($value) : $value);
        }
    }

    public function __set(string $name, $value)
    {
        $this->setData([$name => $value]);
    }

    public function __get(string $name)
    {
        if ($this->offsetExists($name)) {
            return $this->offsetGet($name);
        }
        return null;
    }

    /**
     * Recursive json serializable
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        $records = [];

        foreach (get_object_vars($this) as $key => $value) {
            if (gettype($value) == "object" and method_exists($value, "jsonSerialize")) {
                $records[$key] = $value->jsonSerialize();
            } else {
                $records[$key] = $value;
            }
        }

        return $records;
    }

    /**
     * Convert NativeArray to assoc array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getArrayCopy();
    }
}
