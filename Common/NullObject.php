<?php

declare(strict_types=1);

namespace Microframe\Common;
/**
 * Hide any call on this class
 * collect called functionnames and params
 */
class NullObject
{
    /**
     * Called function container
     *
     * @var array
     */
    private static array $called = [];

    public function __call($name, $arguments): self
    {
        self::$called[$name][] = $arguments;

        return $this;
    }

    public static function __callStatic($name, $arguments): void
    {
        self::$called[$name][] = $arguments;
    }

    /**
     * Determine a function called on this object
     *
     * @param string $name
     * @return boolean
     */
    public function isCalled(string $name): bool
    {
        return isset(self::$called[$name]);
    }

    /**
     * Get a called function parameters
     *
     * @param string $name
     * @return array|null
     */
    public function getCalled(string $name): ?array
    {
        return self::$called[$name] ?? null;
    }
}