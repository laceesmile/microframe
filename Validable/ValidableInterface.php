<?php

declare(strict_types=1);

namespace Microframe\Validable;

interface ValidableInterface
{
    public function validators(): void;

    public function validate(array $source);
}