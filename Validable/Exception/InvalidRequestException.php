<?php

declare(strict_types=1);

namespace Microframe\Validable\Exception;

use Microframe\Common\Exception\MicroframeException;

class InvalidRequestException extends MicroframeException
{
    public function __construct(string $message, int $code = 500)
    {
        $this->code = $code;
        $this->message = $message;
    }
}