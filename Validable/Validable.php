<?php

declare(strict_types=1);

namespace Microframe\Validable;

use Microframe\DI;
use Microframe\Html\Flash;
use Microframe\Translator\Translator;
use Microframe\Translator\TranslatorInterface;
use Microframe\Validator\ValidatorInterface;

class Validable
{
    use Flash;

    public $value;

    private array $validators = [];
    private string $name;

    private TranslatorInterface $trans;

    public final function __construct(string $name, $value)
    {
        $di = new DI();
        $this->trans = $di->has("translator") ? $di->get("translator") : new Translator();

        $this->name = $name;
        $this->value = $value;
    }

    protected function addValidator(ValidatorInterface $validator): self
    {
        $this->validators[] = $validator;

        return $this;
    }

    public function addValidators(array $validators): self
    {
        foreach ($validators as $validator) {
            $this->addValidator($validator);
        }

        return $this;
    }

    public function validate(): bool
    {
        if (empty($this->validators)) {
            return true;
        }

        foreach ($this->validators as $validator) {
            $emptyValid = $validator->checkAllowEmpty($this);
            if (!$emptyValid) {
                $errorClass = $validator->getConfig()->errorClass;
                $message = $validator->getMessage();
                $this->setMessage(new $errorClass($this->trans->_($message, ['name' => ucfirst($this->name)])));
                return false;
            } elseif (!$validator->isEmpty($this->getValue()) and !$validator->validate($this->getValue())) {
                $errorClass = $validator->getConfig()->errorClass;
                $message = $validator->getMessage(false);
                $this->setMessage(new $errorClass($this->trans->_($message, $validator->getMessageParams() + ['name' => ucfirst($this->name)])));
                return false;
            }
        }
        return true;
    }

    public function getValue()
    {
        return $this->value ?? null;
    }
}