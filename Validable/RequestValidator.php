<?php

declare(strict_types=1);

namespace Microframe\Validable;

use Microframe\Helper\Setter;
use Microframe\Validable\Exception\InvalidRequestException;
use Microframe\Validable\ValidableInterface;
use Microframe\Validator\Validator;

abstract class RequestValidator implements ValidableInterface
{
    use Setter;

    private array $validators = [];
    private bool $isValid = true;

    public function __construct()
    {
        $this->validators();
    }

    public function getValidatorsForField(string $name): array
    {
        return $this->validators[$name] ?? [];
    }

    public function getValidators(): array
    {
        return $this->validators ?? [];
    }

    public function clean()
    {
        unset($this->isValid);
        unset($this->validators);
    }

    public function inValid()
    {
        $this->isValid = false;
    }

    /**
     * Validate the resource array
     *
     * @param array $resource
     * @return void
     */
    public function validate(array $resource): void
    {
        foreach ($this->validators as $name => $validators) {
            $validator = (new Validable($name, $resource[$name] ?? null))->addValidators($validators);
                if (!$validator->validate()) {
                    throw new InvalidRequestException($validator->getMessageString(), 400);
                }
        }
    }

    /**
     * Add a validator to a request value.
     *
     * @param string $name
     * @param Validator $validator
     * @return void
     */
    protected function addValidator(string $name, Validator $validator)
    {
        $this->validators[$name][] = $validator;
    }
}