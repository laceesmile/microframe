<?php

declare(strict_types=1);

namespace Microframe\Mvc\View;

use Microframe\Html\Element\Js;
use Microframe\Html\Element\Css;
use Microframe\Html\Element\InnerJs;
use Microframe\Html\Element\InnerCss;

class Assets
{
    private array $css = [];
    private array $js = [];

    private string $assetsPath;

    private string $cssSubPath = "css";
    private string $cssExtension = "css";
    private ?string $cssVersion = null;

    private string $jsSubPath = "js";
    private string $jsExtension = "js";
    private ?string $jsVersion = null;


    public function __construct(string $assetsPath = "/assets")
    {
        $this->assetsPath = $assetsPath;
    }

    public function noCacheJs(?string $version = null, string $prefix = "v"): self
    {
        if (!$version) {
            $version = rand();
        }
        $this->jsVersion = "?" . $prefix . "=" . $version;

        return $this;
    }

    public function noCacheCss(?string $version = null, string $prefix = "v"): self
    {
        if (!$version) {
            $version = rand();
        }
        $this->cssVersion = "?" . $prefix . "=" . $version;

        return $this;
    }

    /**
     * Set the value of css
     *
     * @return  self
     */
    public function addCss(string $css, bool $isLocal = true, array $options = []): self
    {
        $this->css[] = $this->generateCss($css, $isLocal, $options);
        return $this;
    }

    public function innerCss(string $css, bool $isLocal = true, array $options = []): self
    {
        $path = $css;
        if ($isLocal) {
            $path = realpath($this->getFullCssPath($css));
        }
        $content = file_get_contents($path);
        $this->css[] = new InnerCss($content, $options);
        return $this;
    }

    private function getFullCssPath(string $css): string
    {
        return $this->getCssPath() . $css . "." . $this->cssExtension . $this->cssVersion;
    }

    private function generateCss(string $css, bool $isLocal = true, array $options = []): Css
    {
        $options["rel"] = $options["rel"] ?? "stylesheet";
        $options["type"] = $options["type"] ?? "text/css";
        if ($isLocal) {
            $options["href"] = $this->getFullCssPath($css);
        } else {
            $options["href"] = $css;
        }
        return new Css($options);
    }

    public function addPreCss(string $css, bool $isLocal = true, array $options = []): self
    {
        array_unshift($this->css, $this->generateCss($css, $isLocal, $options));
        return $this;
    }

    /**
     * Add value of js
     * @param string required JS file name
     * @param bool optional js file is local or full link
     *
     * @return  self
     */
    public function addJs(string $js, bool $isLocal = true, array $options = []): self
    {
        $this->js[] = $this->generateJs($js, $isLocal, $options);
        return $this;
    }

    public function innerJs(string $js, bool $isLocal = true): self
    {
        if ($isLocal) {
            $content = include_once $js;
        } else {
            $content = file_get_contents($js);
        }
        $js = new InnerJs($content);

        return $this;
    }

    private function getFullJsPath(string $js): string
    {
        return $this->getJsPath() . $js . "." . $this->jsExtension . $this->jsVersion;
    }

    private function generateJs(string $js, bool $isLocal = true, array $options = []): Js
    {
        $options["type"] = $options["type"] ?? "text/javascript";
        if ($isLocal) {
            $options["src"] = $this->getFullJsPath($js);
        } else {
            $options["src"] = $js;
        }
        return new Js($options);
    }

    public function addPreJs(string $js, bool $isLocal = true, array $options = []): self
    {
        array_unshift($this->js, $this->generateJs($js, $isLocal, $options));
        return $this;
    }

    /**
     * Get the value of js
     */
    public function getJs(): string
    {
        return join("", array_unique($this->js));
    }

    /**
     * Get the value of css
     */
    public function getCss(): string
    {
        return join("", array_unique($this->css));
    }

    public function clear(): void
    {
        $this->css = [];
        $this->js = [];
    }

    private function getCssPath(): string
    {
        return join("/", [$this->getAssetsPath(), $this->getCssSubPath()]) . "/";
    }

    private function getJsPath(): string
    {
        return join("/", [$this->getAssetsPath(), $this->getJsSubPath()]) . "/";
    }

    /**
     * Set the value of assetsPath
     */
    public function setAssetsPath(string $assetsPath): self
    {
        $this->assetsPath = $assetsPath;
        
        return $this;
    }

    /**
     * Get the value of assetsPath
     */
    public function getAssetsPath(): string
    {
        return $this->assetsPath;
    }

    /**
     * Get the value of cssSubPath
     */
    public function getCssSubPath(): string
    {
        return $this->cssSubPath;
    }

    /**
     * Set the value of cssSubPath
     *
     * @return  self
     */
    public function setCssSubPath(string $cssSubPath): self
    {
        $this->cssSubPath = $cssSubPath;

        return $this;
    }

    /**
     * Get the value of jsSubPath
     */
    public function getJsSubPath(): string
    {
        return $this->jsSubPath;
    }

    /**
     * Set the value of jsSubPath
     *
     * @return  self
     */
    public function setJsSubPath(string $jsSubPath): self
    {
        $this->jsSubPath = $jsSubPath;

        return $this;
    }
}
