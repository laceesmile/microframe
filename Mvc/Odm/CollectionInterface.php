<?php

declare(strict_types=1);

namespace Microframe\Mvc\Odm;

interface CollectionInterface
{
    public function setUp(): void;
}