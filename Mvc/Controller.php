<?php

declare(strict_types=1);

namespace Microframe\Mvc;

use Microframe\DI;
use Microframe\Helper\Helper;
use Microframe\Helper\RequiredArgsConstructor;
use Microframe\Mvc\Controller\ControllerInterface;

class Controller extends DI implements ControllerInterface
{
    use Helper;
    use RequiredArgsConstructor;
    
    public final function autoloaded()
    {
        $this->initialize();
    }

    public function initialize(){}

}