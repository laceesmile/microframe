<?php

declare(strict_types=1);

namespace Microframe\Mvc;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Microframe\Event\EventHandler;
use Microframe\Mvc\Model\ModelInterface;
use Microframe\Mvc\Model\NullEvent;

abstract class Model extends Eloquent implements ModelInterface
{
    public $timestamps = false;

    protected static $eventClass = null;
    private static Dispatcher $eventHandler;

    protected static $tableName;

    public function __construct(array $params = [])
    {
        $this->setUp();
        $this->bootIfNotBooted();

        $this->syncOriginal();

        $this->fill($params);
        
        self::$tableName = $this->getTable();
    }

    protected static function boot()
    {
        if (static::$eventClass) {
            if (!self::$eventHandler) {
                self::$eventHandler = new EventHandler();
            }
            static::setEventDispatcher(self::$eventHandler);
            static::observe(static::$eventClass);
        }
        parent::boot();
    }

    protected function setPrimaryKey(string $columnName)
    {
        $this->primaryKey = $columnName;
    }

    protected function useTimeStamps(bool $useTimeStamps = true)
    {
        $this->timestamps = $useTimeStamps;
    }

    public static function getAlias(string $alias): string
    {
        $postFix = " AS " . $alias;
        return self::$tableName.$postFix;
    }

    public static function as(string $alias): Builder
    {
        $model = new static();
        return $model::from($model::getAlias($alias));
    }

    public static function withoutEvents(callable $callback)
    {
        $dispatcher = static::getEventDispatcher();

        if ($dispatcher) {
            static::setEventDispatcher(new NullEvent($dispatcher));
        }

        try {
            return $callback();
        } finally {
            if ($dispatcher) {
                static::setEventDispatcher($dispatcher);
            }
        }
    }
}