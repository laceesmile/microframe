<?php

namespace Microframe\Mvc\Controller;

interface ControllerInterface
{
    public function initialize();
}