<?php

declare(strict_types=1);

namespace Microframe\Mvc\Model;

interface ModelInterface
{
    public function setUp(): void;
}