<?php

declare(strict_types=1);

namespace Microframe\Mvc\Model;

use Microframe\Event\EventHandler;

class NullEvent extends EventHandler
{
    private EventHandler $dispatcher;

    public function __construct(EventHandler $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * Disable dispatching on events
     *
     * @param string|object $event
     * @param array $payload
     * @param boolean $halt
     * @return void
     */
    public function dispatch($event, $payload = [], $halt = false): void
    {
    }

    /**
     * Do not push more event on this model
     *
     * @param string|object $event
     * @param array $payload
     * @return void
     */
    public function push($event, $payload = null): self
    {
        return $this;
    }

    /**
     * Don't dispatch an event.
     *
     * @param  string|object  $event
     * @param  mixed  $payload
     * @return array|null
     */
    public function until($event, $payload = [])
    {
    }

    /**
     * Add event listener
     *
     * @param  \Closure|string|array  $events
     * @param  \Closure|string|null  $listener
     * @return void
     */
    public function listen($events, $listener)
    {
        $this->dispatcher->listen($events, $listener);
    }

    /**
     * Determine if a given event has listeners.
     *
     * @param  string  $eventName
     * @return bool
     */
    public function hasListeners($eventName)
    {
        return $this->dispatcher->hasListeners($eventName);
    }

    /**
     * Register an event subscriber with the dispatcher.
     *
     * @param  object|string  $subscriber
     * @return void
     */
    public function subscribe($subscriber)
    {
        $this->dispatcher->subscribe($subscriber);
    }

    /**
     * Flush a set of pushed events.
     *
     * @param  string  $event
     * @return void
     */
    public function flush($event)
    {
        $this->dispatcher->flush($event);
    }

    /**
     * Remove a set of listeners from the dispatcher.
     *
     * @param  string  $event
     * @return void
     */
    public function forget($event)
    {
        $this->dispatcher->forget($event);
    }

    /**
     * Forget all of the queued listeners.
     *
     * @return void
     */
    public function forgetPushed()
    {
        $this->dispatcher->forgetPushed();
    }

    /**
     * Dynamically pass method calls to the underlying dispatcher.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public function __call(string $method, array $parameters = [])
    {
        //return $this->forwardCallTo($this->dispatcher, $method, $parameters);
    }
}