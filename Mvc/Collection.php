<?php

declare(strict_types=1);

namespace Microframe\Mvc;

use Microframe\DI;
use Microframe\Mvc\Odm\CollectionInterface;

abstract class Collection implements CollectionInterface
{
    protected $connection;

    public function __construct()
    {
        $this->connection = (new DI())->get("nosql");
        $this->setUp();
    }

    public function setTable(string $tableName): void
    {
        $this->connection = $this->connection->$tableName;
    }

    public function __call(string $method, array $parameters = [])
    {
        return $this->connection->$method(...$parameters);
    }
}