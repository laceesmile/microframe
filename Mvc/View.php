<?php

declare(strict_types=1);

namespace Microframe\Mvc;

use Microframe\Common\NativeArray;
use Microframe\Common\RegisterTrait;
use Microframe\Common\ClassBuilder;
use Microframe\DI;
use Microframe\Helper\Helper;
use Microframe\Interfaces\RegistableInterface;
use Microframe\Mvc\View\Assets;
use Microframe\Translator\Translator;

class View extends DI implements RegistableInterface
{
    use Helper, RegisterTrait;

    const DIR_SEP = DIRECTORY_SEPARATOR;

    private string $content = "";
    private ?string $html = null;
    private array $viewParams = [];

    protected ?string $viewFile = null;
    protected ?string $layoutFile = null;
    protected string $viewPath = "";

    private array $extension = ["phtml"];

    private string $calledActionName;
    private bool $disabled = false;

    /**
     * Initialize view engine
     *
     * @param ClassBuilder|void $builder
     */
    public final function __construct(ClassBuilder $builder = null)
    {
        $this->set("assets", new Assets());

        if (!$builder) {
            return;
        }

        if ($this->has(Translator::getRegisterName())) {
            $this->t = $this->get(Translator::getRegisterName());
        }

        $this->calledActionName = $this->request->getMatchedRoute()->getAction();

        if ($builder->has("assetsPath", false)) {
            $this->assets->setAssetsPath($builder->get("assetsPath"));
        }

        foreach ($builder->getAll() as $name => $object) {
            $fnc = "set" . ucfirst($name);
            if (method_exists($this, $fnc)) {
                $this->$fnc($object);
            }
        }
    }

    /**
     * Magic variable setter
     * These variables passed to view by name
     * 
     * @example: $view->set("foo", "bar") - You can reach on view like $foo = "bar";
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set(string $name, $value)
    {
        $this->setVar($name, $value);
    }

    /**
     * Render the view
     *
     * @return void
     */
    public final function render()
    {
        echo $this->build();
    }

    /**
     * Build the view if not disabled
     *
     * @return string|null
     */
    public function build(): ?string
    {
        if ($this->getDisabled()) {
            return null;
        }
        extract($this->viewParams);

        $this->processRender();

        if (!is_null($this->viewFile)) {
            ob_start();
            include $this->viewFile;
            $this->addContent(ob_get_clean());
        }

        if (!is_null($this->layoutFile)) {
            ob_start();
            include $this->layoutFile;
            $this->html = ob_get_clean();
        }
        return preg_replace(['/(\>\s+)/', '/(\s+\<)/'], [">", "<"], ($this->html ?? $this->content));
    }

    /**
     * Build a view file
     *
     * @param string $filePath fullpath or search in current view path
     * @param array $params viewparams
     * @return void
     */
    public function buildFile(string $filePath, array $params = [])
    {
        ob_start();
        $this->part($filePath, $params);
        return ob_get_clean();
    }

    private function processRender(): void
    {
        $this->layoutFile = $this->getFile($this->layoutFile);

        $viewFile = $this->viewFile ?? $this->calledActionName;
        $viewFile = $this->getViewPath() . $viewFile;

        $this->viewFile = $this->getFile($viewFile);
    }

    private function getFile($file): ?string
    {
        if (empty($file)) {
            return null;
        }
        $file = $this->fixFilePath($file);
        foreach ($this->extension as $ext) {
            $tmp = $file . "." . $ext;
            if (file_exists($this->getViewPath() . $tmp)) {
                return $this->getViewPath() . $tmp;
            } elseif (file_exists($tmp)) {
                return $tmp;
            }
        }
        return null;
    }

    private function setExtension($extension = null): void
    {
        if (!is_array($extension)) {
            $extension = [$extension];
        }
        $this->extension = array_unique($extension);
    }

    /**
     * Set variable to view
     *
     * @param string $name
     * @param mixed $value
     * @return void
     */
    public final function setVar(string $name, $value)
    {
        $this->viewParams[$name] = $value;
    }

    /**
     * Add multiple view params
     * @call setVar
     *
     * @param array $valueList
     * @return void
     */
    public final function setVars(array|NativeArray $valueList)
    {
        foreach ($valueList as $name => $value) {
            $this->setVar($name, $value);
        }
    }

    public function addContent(string $html = ""): void
    {
        $this->content .= $html;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function pick($file): self
    {
        $this->viewFile = $file;

        return $this;
    }

    /**
     * Disable the view
     *
     * @return boolean
     */
    public function disable(): bool
    {
        $this->disabled = true;

        return true;
    }

    /**
     * Enable the view
     *
     * @return boolean
     */
    public function enable(): bool
    {
        $this->disabled = false;

        return true;
    }

    /**
     * Erease only the content, keeps layout in memory
     *
     * @return boolean
     */
    public function eraseContent(): bool
    {
        $this->content = "";

        return true;
    }

    /**
     * Clear the full content
     *
     * @return boolean
     */
    public function clear(): bool
    {
        $this->viewParams = [];
        $this->eraseContent();
        $this->viewFile = null;
        $this->layoutFile = null;
        $this->assets->clear();
        $this->disabled = false;

        return true;
    }

    /**
     * Insert a viewfile
     *
     * @param string $path fullpath or search in current view path
     * @param array $vars vars to part file
     * @return void
     */
    public function part(string $path, array $vars = [])
    {
        if (($file = $this->getFile($path)) !== null) {
            if (empty($vars)) {
                $vars = $this->viewParams;
            }
            extract($vars);
            include $file;
        }
    }

    /**
     * Get the value of layoutFile
     */
    public function getLayout()
    {
        return $this->layoutFile;
    }

    /**
     * Set the value of layoutFile
     *
     * @return  self
     */
    public function setLayout(?string $layoutFile)
    {
        $this->layoutFile = $layoutFile;

        return $this;
    }

    /**
     * Get the value of viewPath
     */
    public function getViewPath(): string
    {
        return $this->viewPath ? rtrim($this->viewPath, self::DIR_SEP) . self::DIR_SEP : '';
    }

    /**
     * Set the value of viewPath
     *
     * @return  self
     */
    public function setViewPath($viewPath)
    {
        $this->viewPath = rtrim($viewPath, self::DIR_SEP) . self::DIR_SEP;

        return $this;
    }

    /**
     * Get the value of disabled
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    private function fixFilePath(string $path): string
    {
        return join(self::DIR_SEP, preg_split("/\//", $path));
    }
}
